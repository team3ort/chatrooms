﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace ServerChatRooms
{
    class ServerHttp
    {
        HttpListener listener = new HttpListener();
        string filePath = @"D:/GoogleDisk/Prog/Servers/Http_server/HttpServer/HttpServer/www/";        

        public ServerHttp(string host = "http://sealaunch.ddns.net:8080/")
        {
            listener.Prefixes.Add(host);
        }

        public void StartServer()
        {
            listener.Start();
            Console.WriteLine("Server start");
            while (true)
            {
                HttpListenerContext context = listener.GetContext();
                HttpListenerRequest request = context.Request;
                HttpListenerResponse response = context.Response;
                
                if (request.HttpMethod == "POST")
                {                    
                    String postReq = new StreamReader(request.InputStream).ReadToEnd();

                    Console.WriteLine("Client method: {0}\tRequest: {1}", request.HttpMethod, postReq);     // УБРАТЬ

                    SendResponse(postReq, response);                    
                }
            }
        }

        private void SendResponse(String data, HttpListenerResponse response)
        {
            String responseStr = CommandDAO.GerResponse(data);
            Console.WriteLine(responseStr);

            Stream responseStraem;
                        
            byte[] buff = Encoding.Default.GetBytes(responseStr);            
            
            responseStraem = response.OutputStream;
            responseStraem.Write(buff, 0, buff.Length);
            responseStraem.Dispose();
        }
    }
}