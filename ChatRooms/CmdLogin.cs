﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ChatRooms
{
    class CmdLogin : ICommand<User>
    {
        User user;
        Dictionary<string, string> request = new Dictionary<string, string>();
        public CmdLogin(User user)
        {
            this.user = user;
            request.Add("cmd", "login");
            request.Add("login", user.Login);
            request.Add("pass", user.Password);
        }

        public User GetResponse()
        {
            string response = new Client(request).GetResponse();
            try
            {
                User user = JsonConvert.DeserializeObject<User>(response);
                return user;
            }
            catch (JsonSerializationException e)
            {
                try
                {
                    ArgumentException ex = JsonConvert.DeserializeObject<ArgumentException>(response);
                    throw ex;
                }
                catch(JsonSerializationException e1)
                {
                    throw new DataMisalignedException(); /// редкое исключение временно пока нет своих
                }
            }
        }

        
    }
}
