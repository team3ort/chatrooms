﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using ChatModels;

namespace ChatRooms
{
    class ChatClient
    {
        public TcpClient tcpClient = new TcpClient();
        public User User { get; set; }

        public ChatClient(string host, int port)
        {
            tcpClient.Connect(host, port);

        }
    }
}
