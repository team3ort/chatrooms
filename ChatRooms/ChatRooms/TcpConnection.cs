﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ChatRooms
{
    public class TcpConnection
    {
        
        public void SendRequest(string request, NetworkStream stream)
        {
            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write(request);
            //writer.Flush();
            
        }

        public string GetResponseForRequest(NetworkStream stream, string request)
        {
            BinaryReader reader = new BinaryReader(stream);
            SendRequest(request, stream);
            string result = "";
            result = reader.ReadString();
            return result;
        }

        public string GetResponse(NetworkStream stream)
        {
            BinaryReader reader = new BinaryReader(stream);
            string result = "";
            result = reader.ReadString();
            return result;
        }
    }
}
