﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChatModels;

namespace ChatRooms
{
    public class ChatEngine
    {
        public bool _isMessaging = false;
        AreModule areModule = new AreModule();
        ProfileModule pfModule = new ProfileModule();
        MessageModule msgModule;
        RoomMenagerModule rmModule;

        public User LogIn(string login, string password)
        {
            User loginUser = new User(login, password);
            return areModule.LogIn(loginUser);
        }

        public bool Register(string login, string password, string email,
                             string fName, string lName, string phone)
        {
            User regUser = new User(login, password, fName, lName, email, phone);
            return areModule.Register(regUser);
        }

        public bool Forget(string email)
        {
            return areModule.Forget(email);
        }

        public void InitMessageModule(UserMainForm form)
        {
            msgModule = new MessageModule(form);
        }

        public void InitRoomMenagerModule()
        {
            rmModule = new RoomMenagerModule();
        }

        public void GetRooms()
        {
            rmModule.GetRooms();
        }

        public void StartMessaging()
        {
            if (!_isMessaging)
            {
                msgModule.Run();
                _isMessaging = true;
            }
        }

        public void StopMessaging()
        {
            if (_isMessaging)
            {
                msgModule.Stop();
                _isMessaging = false;
            }
        }

        public void EnterInRoom(Room room)
        {
            rmModule.EnterInRoom(room);
        }

        public void LeaveRoom(Room room)
        {
            rmModule.LeaveRoom(room);
        }

        public void SendMessage(string message, Room room)
        {
            msgModule.SendMessage(message, room);
        }

        public void CreateRoom(Room room)
        {
            rmModule.CreateRoom(room);
        }

        public void ChangeUserPassword(string password)
        {
            User newUser = Program.chatClient.User.Clone();
            newUser.Password = password;
            Program.chatClient.User = pfModule.ChangeProfile(newUser);
        }
    }
}
