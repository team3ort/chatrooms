﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;

namespace ChatRooms
{
    public class ProfileModule
    {
        public User ChangeProfile(User user)
        {
            CmdChangeProfile cmd = new CmdChangeProfile(user);
            return cmd.GetResponse();
        }
    }
}
