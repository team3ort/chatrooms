﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ChatModels;
using Newtonsoft.Json;
using System.Net.Sockets;

namespace ChatRooms
{
    public class MessageModule
    {
        UserMainForm form;
        ReciveResponseParser rrp;
        IntefaceAdapter adapter;
        public bool _isReceiving = false;
        Thread receivingThread;

        public MessageModule(UserMainForm form)
        {
            this.form = form;
        }

        

        public void Run()
        {
            _isReceiving = true;
            adapter = new IntefaceAdapter(form);
            rrp = new ReciveResponseParser(adapter);
            receivingThread = new Thread(Receive);
            receivingThread.IsBackground = true;
            receivingThread.Start();
        }

        public void Stop()
        {
            _isReceiving = false;
            receivingThread.Abort();
        }

        private void Receive()
        {
            NetworkStream stream = Program.chatClient.tcpClient.GetStream();
            while (_isReceiving)
            {
                if (stream.DataAvailable)
                {
                    string respStr = Program.connection.GetResponse(stream);
                    ResponsePackage rp = JsonConvert.DeserializeObject<ResponsePackage>(respStr);
                    rrp.DoWork(rp);
                }
            }
        }

        

        public void SendMessage(string message, Room room)
        {
            Message msg = new Message(Program.chatClient.User, room, message);
            CmdSendMessage cmd = new CmdSendMessage(msg);
            cmd.SendRequest();
        }
    }
}
