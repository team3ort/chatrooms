﻿using ChatModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRooms
{
    public class RoomMenagerModule
    {


        public void EnterInRoom(Room room)
        {
            CmdEnterInRoom cmd = new CmdEnterInRoom(room);
            cmd.SendRequest();
        }

        public void LeaveRoom(Room room)
        {
            CmdLeaveRoom cmd = new CmdLeaveRoom(room);
            cmd.SendRequest();
        }

        public void GetRooms()
        {
            CmdGetRooms cmd = new CmdGetRooms();
            cmd.SendRequest();
        }

        public void CreateRoom(Room room)
        {
            CmdCreateRoom cmd = new CmdCreateRoom(room);
            cmd.SendRequest();
        }
    }
}
