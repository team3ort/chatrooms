﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;
using Newtonsoft.Json;

namespace ChatRooms
{
    class CmdLogin
    {
        User user;
        Dictionary<string, string> request;
        public CmdLogin(User user)
        {
            this.user = user;
        }

        public User GetResponse()
        {
            request = new Dictionary<string, string>();
            request.Add("cmd", "login");
            request.Add("login", user.Login);
            request.Add("password", user.Password);

            string reqString = JsonConvert.SerializeObject(request);
            string respString = Program.connection.GetResponseForRequest(Program.chatClient.tcpClient.GetStream(), reqString);
            ResponsePackage rp = JsonConvert.DeserializeObject<ResponsePackage>(respString);
            if (rp.DataType == "user")
            {
                User res = JsonConvert.DeserializeObject<User>(rp.Data);
                return res;
            }
            else if (rp.DataType == "error")
            {
                Error er = JsonConvert.DeserializeObject<Error>(rp.Data);
                throw new ArgumentException(er.Message);
            }
            throw new ArgumentException("Somesing wrong");

        }
    }
}
