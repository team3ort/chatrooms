﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;
using Newtonsoft.Json;

namespace ChatRooms
{
    public class CmdEnterInRoom
    {
        Room room;
        Dictionary<string, string> request;
        public CmdEnterInRoom(Room room)
        {
            this.room = room;
        }

        public void SendRequest()
        {
            request = new Dictionary<string, string>();
            request.Add("cmd", "userenterinroom");
            request.Add("room", room.Name);

            string reqString = JsonConvert.SerializeObject(request);
            Program.connection.SendRequest(reqString, Program.chatClient.tcpClient.GetStream());

        }
    }
}
