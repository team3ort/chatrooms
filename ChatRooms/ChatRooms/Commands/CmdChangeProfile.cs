﻿using ChatModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRooms
{
    public class CmdChangeProfile
    {
        User user;
        public CmdChangeProfile(User user)
        {
            this.user = user;
        }

        Dictionary<string, string> request;
        public User GetResponse()
        {
            request = new Dictionary<string, string>();
            request.Add("cmd", "changeprofile");
            request.Add("login", user.Login);
            request.Add("password", user.Password);
            request.Add("email", user.Email);
            request.Add("fname", user.FirstName);
            request.Add("lname", user.LastName);
            request.Add("phone", user.Phone);

            string reqString = JsonConvert.SerializeObject(request);
            string respString = Program.connection.GetResponseForRequest(Program.chatClient.tcpClient.GetStream(), reqString);
            ResponsePackage rp = JsonConvert.DeserializeObject<ResponsePackage>(respString);
            if (rp.DataType == "user")
            {
                User res = JsonConvert.DeserializeObject<User>(rp.Data);
                return res;
            }
            else if (rp.DataType == "error")
            {
                Error er = JsonConvert.DeserializeObject<Error>(rp.Data);
                throw new ArgumentException(er.Message);
            }
            throw new ArgumentException("Somesing wrong");

        }
    }
}
