﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;

namespace ChatRooms
{
    public class CmdForget
    {
        string email;
        Dictionary<string, string> request;
        public CmdForget(string email)
        {
            this.email = email;
        }

        public bool GetResponse()
        {
            request = new Dictionary<string, string>();
            request.Add("cmd", "forget");
            request.Add("email", email);

            string reqString = JsonConvert.SerializeObject(request);
            string respString = Program.connection.GetResponseForRequest(Program.chatClient.tcpClient.GetStream(), reqString);
            ResponsePackage rp = JsonConvert.DeserializeObject<ResponsePackage>(respString);
            if (rp.DataType == "bool")
            {
                bool res = JsonConvert.DeserializeObject<bool>(rp.Data);
                return res;
            }
            else if (rp.DataType == "error")
            {
                Error er = JsonConvert.DeserializeObject<Error>(rp.Data);
                throw new ArgumentException(er.Message);
            }
            throw new ArgumentException("Somesing wrong");

        }
    }
}
