﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;
using Newtonsoft.Json;

namespace ChatRooms
{
    public class CmdGetRooms
    {
        Dictionary<string, string> request;
        public CmdGetRooms()
        {

        }

        public void SendRequest()
        {
            request = new Dictionary<string, string>();
            request.Add("cmd", "getallrooms");

            string reqString = JsonConvert.SerializeObject(request);
            Program.connection.SendRequest(reqString, Program.chatClient.tcpClient.GetStream());

        }
    }
}
