﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;
using Newtonsoft.Json;

namespace ChatRooms
{
    public class CmdRegister
    {
        User user;
        Dictionary<string, string> request;
        public CmdRegister(User user)
        {
            this.user = user;
        }

        public bool GetResponse()
        {
            request = new Dictionary<string, string>();
            request.Add("cmd", "register");
            request.Add("login", user.Login);
            request.Add("password", user.Password);
            request.Add("email", user.Email);
            request.Add("fname", user.FirstName);
            request.Add("lname", user.LastName);
            request.Add("phone", user.Phone);

            string reqString = JsonConvert.SerializeObject(request);
            string respString = Program.connection.GetResponseForRequest(Program.chatClient.tcpClient.GetStream(), reqString);
            ResponsePackage rp = JsonConvert.DeserializeObject<ResponsePackage>(respString);
            if (rp.DataType == "bool")
            {
                bool res = JsonConvert.DeserializeObject<bool>(rp.Data);
                return res;
            }
            else if (rp.DataType == "error")
            {
                Error er = JsonConvert.DeserializeObject<Error>(rp.Data);
                throw new ArgumentException(er.Message);
            }
            throw new ArgumentException("Somesing wrong");

        }
    }
}
