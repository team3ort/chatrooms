﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;
using Newtonsoft.Json;

namespace ChatRooms
{
    public class CmdSendMessage
    {
        Message msg;
        Dictionary<string, string> request;
        public CmdSendMessage(Message msg)
        {
            this.msg = msg;
        }

        public void SendRequest()
        {
            request = new Dictionary<string, string>();
            request.Add("cmd", "message");
            request.Add("author", msg.Author.Login);
            request.Add("roomname", msg.Room.Name);
            request.Add("msgdata", msg.MsgData);


            string reqString = JsonConvert.SerializeObject(request);

            Program.connection.SendRequest(reqString, Program.chatClient.tcpClient.GetStream());

        }
    }
}
