﻿namespace ChatRooms
{
    partial class PassRecoveryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PassRecoveryForm));
            this.lblRecPass = new System.Windows.Forms.Label();
            this.pnlPassRecovery = new System.Windows.Forms.Panel();
            this.btnSend = new System.Windows.Forms.Button();
            this.lblEMail = new System.Windows.Forms.Label();
            this.txtEMail = new System.Windows.Forms.TextBox();
            this.pnlPassRecovery.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblRecPass
            // 
            this.lblRecPass.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblRecPass.AutoSize = true;
            this.lblRecPass.BackColor = System.Drawing.Color.Transparent;
            this.lblRecPass.Location = new System.Drawing.Point(343, 13);
            this.lblRecPass.Name = "lblRecPass";
            this.lblRecPass.Size = new System.Drawing.Size(124, 13);
            this.lblRecPass.TabIndex = 0;
            this.lblRecPass.Text = "Восстановлене пароля";
            // 
            // pnlPassRecovery
            // 
            this.pnlPassRecovery.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlPassRecovery.BackColor = System.Drawing.Color.Transparent;
            this.pnlPassRecovery.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPassRecovery.Controls.Add(this.btnSend);
            this.pnlPassRecovery.Controls.Add(this.lblEMail);
            this.pnlPassRecovery.Controls.Add(this.txtEMail);
            this.pnlPassRecovery.Location = new System.Drawing.Point(219, 53);
            this.pnlPassRecovery.Name = "pnlPassRecovery";
            this.pnlPassRecovery.Size = new System.Drawing.Size(383, 102);
            this.pnlPassRecovery.TabIndex = 1;
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(177, 51);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 2;
            this.btnSend.Text = "Отправить";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // lblEMail
            // 
            this.lblEMail.AutoSize = true;
            this.lblEMail.Location = new System.Drawing.Point(84, 28);
            this.lblEMail.Name = "lblEMail";
            this.lblEMail.Size = new System.Drawing.Size(87, 13);
            this.lblEMail.TabIndex = 1;
            this.lblEMail.Text = "Почтовый ящик";
            // 
            // txtEMail
            // 
            this.txtEMail.Location = new System.Drawing.Point(177, 25);
            this.txtEMail.Name = "txtEMail";
            this.txtEMail.Size = new System.Drawing.Size(154, 20);
            this.txtEMail.TabIndex = 0;
            this.txtEMail.Text = "Enter your data";
            // 
            // PassRecoveryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(817, 465);
            this.Controls.Add(this.pnlPassRecovery);
            this.Controls.Add(this.lblRecPass);
            this.DoubleBuffered = true;
            this.Name = "PassRecoveryForm";
            this.Text = "Chat";
            this.pnlPassRecovery.ResumeLayout(false);
            this.pnlPassRecovery.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRecPass;
        private System.Windows.Forms.Panel pnlPassRecovery;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Label lblEMail;
        private System.Windows.Forms.TextBox txtEMail;
    }
}