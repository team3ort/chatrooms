﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChatModels.Exceptions;

namespace ChatRooms
{
    public partial class PassRecoveryForm : Form
    {
        Form parrent;
        bool res = false;
        public PassRecoveryForm(Form parrent)
        {
            this.parrent = parrent;
            InitializeComponent();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            String emailStr = txtEMail.Text;
            res = Program.engine.Forget(emailStr);
            try
            {
                if (res)
                {
                    MessageBox.Show("Password send to you email");
                    parrent.Show();
                    this.Close();
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
