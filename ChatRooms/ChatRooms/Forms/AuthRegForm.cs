﻿using ChatModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatRooms
{
    public partial class AuthRegForm : Form
    {
        string[] txtFields = { "FirstName", "LastName", "NickName", "Mail", "PhoneNumber", "Password" };

        public AuthRegForm()
        {
            InitializeComponent();


        }

        

        

        private void pnlLoginBtn_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                Program.engine.LogIn(txtAuthLogin.Text, txtAuthPassword.Text);
                UserMainForm form = new UserMainForm(this);
                form.Show();
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lblForgotPassBtn_Click(object sender, EventArgs e)
        {
            PassRecoveryForm passRec = new PassRecoveryForm(this);
            passRec.Show();
            Hide();
        }


        private void Register_Click(object sender, EventArgs e)
        {
            try
            {
                bool response = Program.engine.Register(txtLogin.Text, txtPass.Text, txtEmail.Text,
                     txtFirstName.Text, txtLastName.Text, txtPhone.Text);
                RegConfirmForm form = new RegConfirmForm(this);
                form.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void TextBoxEnter(TextBox txtBox, string str)
        {
            if (txtBox.Text == str)
                txtBox.Text = "";
        }
        private void TextBoxLeave(TextBox txtBox, string str)
        {
            if (txtBox.Text == "")
                txtBox.Text = str;
        }

        private void txtFirstName_Enter(object sender, EventArgs e)
        {
            TextBoxEnter((TextBox)sender, txtFields[0]);
        }

        private void txtFirstName_Leave(object sender, EventArgs e)
        {
            TextBoxLeave((TextBox)sender, txtFields[0]);
        }

        private void txtLastName_Enter(object sender, EventArgs e)
        {
            TextBoxEnter((TextBox)sender, txtFields[1]);
        }

        private void txtLastName_Leave(object sender, EventArgs e)
        {
            TextBoxLeave((TextBox)sender, txtFields[1]);
        }

        private void txtLogin_Enter(object sender, EventArgs e)
        {
            TextBoxEnter((TextBox)sender, txtFields[2]);
        }

        private void txtLogin_Leave(object sender, EventArgs e)
        {
            TextBoxLeave((TextBox)sender, txtFields[2]);
        }

        private void txtEmail_Enter(object sender, EventArgs e)
        {
            TextBoxEnter((TextBox)sender, txtFields[3]);
        }

        private void txtEmail_Leave(object sender, EventArgs e)
        {
            TextBoxLeave((TextBox)sender, txtFields[3]);
        }

        private void txtPhone_Enter(object sender, EventArgs e)
        {
            TextBoxEnter((TextBox)sender, txtFields[4]);
        }

        private void txtPhone_Leave(object sender, EventArgs e)
        {
            TextBoxLeave((TextBox)sender, txtFields[4]);
        }

        private void txtPass_Enter(object sender, EventArgs e)
        {
            TextBoxEnter((TextBox)sender, txtFields[5]);
        }

        private void txtPass_Leave(object sender, EventArgs e)
        {
            TextBoxLeave((TextBox)sender, txtFields[5]);
        }

        public void getCommandFromUI(object o, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
