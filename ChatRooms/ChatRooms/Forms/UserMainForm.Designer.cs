﻿namespace ChatRooms
{
    partial class UserMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserMainForm));
            this.lblPrivInf = new System.Windows.Forms.Label();
            this.pnlPrivInf = new System.Windows.Forms.Panel();
            this.lblMailInfo = new System.Windows.Forms.Label();
            this.lblPhoneInfo = new System.Windows.Forms.Label();
            this.lblLNameInfo = new System.Windows.Forms.Label();
            this.lblFnameInfo = new System.Windows.Forms.Label();
            this.btnDnlPhoto = new System.Windows.Forms.Button();
            this.pnlPhoto = new System.Windows.Forms.Panel();
            this.pnlUsers = new System.Windows.Forms.Panel();
            this.lvUsers = new System.Windows.Forms.ListView();
            this.pnlComplainBtn = new System.Windows.Forms.Panel();
            this.lblUserList = new System.Windows.Forms.Label();
            this.pnlRooms = new System.Windows.Forms.Panel();
            this.lvRooms = new System.Windows.Forms.ListView();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.pnlAddRoomBtn = new System.Windows.Forms.Panel();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lblRooms = new System.Windows.Forms.Label();
            this.pnlRoomMsg = new System.Windows.Forms.Panel();
            this.rtbMsg = new System.Windows.Forms.RichTextBox();
            this.pnlSendMsgBtn = new System.Windows.Forms.Panel();
            this.pnlLeaveRoomBtn = new System.Windows.Forms.Panel();
            this.lblRoomName = new System.Windows.Forms.Label();
            this.txtSendMsg = new System.Windows.Forms.TextBox();
            this.btnChangeLName = new System.Windows.Forms.Button();
            this.txtLName = new System.Windows.Forms.TextBox();
            this.lblLName = new System.Windows.Forms.Label();
            this.btnChangeFName = new System.Windows.Forms.Button();
            this.txtFName = new System.Windows.Forms.TextBox();
            this.lblFName = new System.Windows.Forms.Label();
            this.btnChangeLogin = new System.Windows.Forms.Button();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.lblLogin = new System.Windows.Forms.Label();
            this.btnChangePhone = new System.Windows.Forms.Button();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.lblPhone = new System.Windows.Forms.Label();
            this.btnChangeMail = new System.Windows.Forms.Button();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.lblMail = new System.Windows.Forms.Label();
            this.btnChangePass = new System.Windows.Forms.Button();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.lblPass = new System.Windows.Forms.Label();
            this.lblUserLogin = new System.Windows.Forms.Label();
            this.pnlExitBtn = new System.Windows.Forms.Panel();
            this.pnlMyPage = new System.Windows.Forms.Panel();
            this.btnMyPage = new System.Windows.Forms.Button();
            this.btnMyDialogs = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.pnlSettings = new System.Windows.Forms.Panel();
            this.pnlMyDialogs = new System.Windows.Forms.Panel();
            this.pnlCreateRoom = new System.Windows.Forms.Panel();
            this.pnlCreateRoomBtn = new System.Windows.Forms.Panel();
            this.txtNewRoomName = new System.Windows.Forms.TextBox();
            this.pnlPrivInf.SuspendLayout();
            this.pnlUsers.SuspendLayout();
            this.pnlRooms.SuspendLayout();
            this.pnlRoomMsg.SuspendLayout();
            this.pnlMyPage.SuspendLayout();
            this.pnlSettings.SuspendLayout();
            this.pnlMyDialogs.SuspendLayout();
            this.pnlCreateRoom.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblPrivInf
            // 
            this.lblPrivInf.AutoSize = true;
            this.lblPrivInf.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPrivInf.Location = new System.Drawing.Point(234, 28);
            this.lblPrivInf.Name = "lblPrivInf";
            this.lblPrivInf.Size = new System.Drawing.Size(254, 17);
            this.lblPrivInf.TabIndex = 3;
            this.lblPrivInf.Text = "Личная информация о пользователе";
            // 
            // pnlPrivInf
            // 
            this.pnlPrivInf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPrivInf.Controls.Add(this.lblMailInfo);
            this.pnlPrivInf.Controls.Add(this.lblPhoneInfo);
            this.pnlPrivInf.Controls.Add(this.lblLNameInfo);
            this.pnlPrivInf.Controls.Add(this.lblFnameInfo);
            this.pnlPrivInf.Location = new System.Drawing.Point(226, 81);
            this.pnlPrivInf.Name = "pnlPrivInf";
            this.pnlPrivInf.Size = new System.Drawing.Size(284, 139);
            this.pnlPrivInf.TabIndex = 2;
            // 
            // lblMailInfo
            // 
            this.lblMailInfo.AutoSize = true;
            this.lblMailInfo.Location = new System.Drawing.Point(4, 47);
            this.lblMailInfo.Name = "lblMailInfo";
            this.lblMailInfo.Size = new System.Drawing.Size(108, 13);
            this.lblMailInfo.TabIndex = 3;
            this.lblMailInfo.Text = "labelПочтовыйЯщик";
            // 
            // lblPhoneInfo
            // 
            this.lblPhoneInfo.AutoSize = true;
            this.lblPhoneInfo.Location = new System.Drawing.Point(4, 33);
            this.lblPhoneInfo.Name = "lblPhoneInfo";
            this.lblPhoneInfo.Size = new System.Drawing.Size(114, 13);
            this.lblPhoneInfo.TabIndex = 2;
            this.lblPhoneInfo.Text = "labelНомерТелефона";
            // 
            // lblLNameInfo
            // 
            this.lblLNameInfo.AutoSize = true;
            this.lblLNameInfo.Location = new System.Drawing.Point(4, 19);
            this.lblLNameInfo.Name = "lblLNameInfo";
            this.lblLNameInfo.Size = new System.Drawing.Size(78, 13);
            this.lblLNameInfo.TabIndex = 1;
            this.lblLNameInfo.Text = "labelФамилия";
            // 
            // lblFnameInfo
            // 
            this.lblFnameInfo.AutoSize = true;
            this.lblFnameInfo.Location = new System.Drawing.Point(4, 5);
            this.lblFnameInfo.Name = "lblFnameInfo";
            this.lblFnameInfo.Size = new System.Drawing.Size(51, 13);
            this.lblFnameInfo.TabIndex = 0;
            this.lblFnameInfo.Text = "labelИмя";
            // 
            // btnDnlPhoto
            // 
            this.btnDnlPhoto.Location = new System.Drawing.Point(32, 226);
            this.btnDnlPhoto.Name = "btnDnlPhoto";
            this.btnDnlPhoto.Size = new System.Drawing.Size(131, 23);
            this.btnDnlPhoto.TabIndex = 1;
            this.btnDnlPhoto.Text = "Загрузить фото";
            this.btnDnlPhoto.UseVisualStyleBackColor = true;
            // 
            // pnlPhoto
            // 
            this.pnlPhoto.BackColor = System.Drawing.SystemColors.Control;
            this.pnlPhoto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlPhoto.BackgroundImage")));
            this.pnlPhoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPhoto.Location = new System.Drawing.Point(7, 22);
            this.pnlPhoto.Name = "pnlPhoto";
            this.pnlPhoto.Size = new System.Drawing.Size(177, 198);
            this.pnlPhoto.TabIndex = 0;
            // 
            // pnlUsers
            // 
            this.pnlUsers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlUsers.Controls.Add(this.lvUsers);
            this.pnlUsers.Controls.Add(this.pnlComplainBtn);
            this.pnlUsers.Controls.Add(this.lblUserList);
            this.pnlUsers.Location = new System.Drawing.Point(25, 262);
            this.pnlUsers.Name = "pnlUsers";
            this.pnlUsers.Size = new System.Drawing.Size(343, 135);
            this.pnlUsers.TabIndex = 2;
            // 
            // lvUsers
            // 
            this.lvUsers.Location = new System.Drawing.Point(3, 25);
            this.lvUsers.Name = "lvUsers";
            this.lvUsers.Size = new System.Drawing.Size(289, 105);
            this.lvUsers.TabIndex = 5;
            this.lvUsers.UseCompatibleStateImageBehavior = false;
            // 
            // pnlComplainBtn
            // 
            this.pnlComplainBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlComplainBtn.BackgroundImage")));
            this.pnlComplainBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlComplainBtn.Location = new System.Drawing.Point(291, 3);
            this.pnlComplainBtn.Name = "pnlComplainBtn";
            this.pnlComplainBtn.Size = new System.Drawing.Size(33, 33);
            this.pnlComplainBtn.TabIndex = 1;
            // 
            // lblUserList
            // 
            this.lblUserList.AutoSize = true;
            this.lblUserList.Location = new System.Drawing.Point(15, 9);
            this.lblUserList.Name = "lblUserList";
            this.lblUserList.Size = new System.Drawing.Size(124, 13);
            this.lblUserList.TabIndex = 0;
            this.lblUserList.Text = "Список пользователей";
            // 
            // pnlRooms
            // 
            this.pnlRooms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRooms.Controls.Add(this.lvRooms);
            this.pnlRooms.Controls.Add(this.pnlAddRoomBtn);
            this.pnlRooms.Controls.Add(this.txtSearch);
            this.pnlRooms.Controls.Add(this.lblRooms);
            this.pnlRooms.Location = new System.Drawing.Point(375, 10);
            this.pnlRooms.Name = "pnlRooms";
            this.pnlRooms.Size = new System.Drawing.Size(156, 238);
            this.pnlRooms.TabIndex = 1;
            // 
            // lvRooms
            // 
            this.lvRooms.Location = new System.Drawing.Point(5, 30);
            this.lvRooms.Name = "lvRooms";
            this.lvRooms.Size = new System.Drawing.Size(138, 176);
            this.lvRooms.SmallImageList = this.imageList;
            this.lvRooms.TabIndex = 4;
            this.lvRooms.UseCompatibleStateImageBehavior = false;
            this.lvRooms.View = System.Windows.Forms.View.SmallIcon;
            this.lvRooms.ItemActivate += new System.EventHandler(this.lvRooms_ItemActivate);
            this.lvRooms.SelectedIndexChanged += new System.EventHandler(this.lvRooms_SelectedIndexChanged);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "409.png");
            this.imageList.Images.SetKeyName(1, "blank.png");
            // 
            // pnlAddRoomBtn
            // 
            this.pnlAddRoomBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlAddRoomBtn.BackgroundImage")));
            this.pnlAddRoomBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlAddRoomBtn.Location = new System.Drawing.Point(117, -2);
            this.pnlAddRoomBtn.Name = "pnlAddRoomBtn";
            this.pnlAddRoomBtn.Size = new System.Drawing.Size(33, 33);
            this.pnlAddRoomBtn.TabIndex = 3;
            this.pnlAddRoomBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlAddRoomBtn_MouseClick);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(33, 212);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(88, 20);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.Text = "Поиск";
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            this.txtSearch.Enter += new System.EventHandler(this.txtSearch_Enter);
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.Leave += new System.EventHandler(this.txtSearch_Leave);
            // 
            // lblRooms
            // 
            this.lblRooms.AutoSize = true;
            this.lblRooms.Location = new System.Drawing.Point(12, 9);
            this.lblRooms.Name = "lblRooms";
            this.lblRooms.Size = new System.Drawing.Size(53, 13);
            this.lblRooms.TabIndex = 0;
            this.lblRooms.Text = "Комнаты";
            // 
            // pnlRoomMsg
            // 
            this.pnlRoomMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRoomMsg.Controls.Add(this.rtbMsg);
            this.pnlRoomMsg.Controls.Add(this.pnlSendMsgBtn);
            this.pnlRoomMsg.Controls.Add(this.pnlLeaveRoomBtn);
            this.pnlRoomMsg.Controls.Add(this.lblRoomName);
            this.pnlRoomMsg.Controls.Add(this.txtSendMsg);
            this.pnlRoomMsg.Location = new System.Drawing.Point(21, 9);
            this.pnlRoomMsg.Name = "pnlRoomMsg";
            this.pnlRoomMsg.Size = new System.Drawing.Size(347, 239);
            this.pnlRoomMsg.TabIndex = 0;
            // 
            // rtbMsg
            // 
            this.rtbMsg.Location = new System.Drawing.Point(3, 31);
            this.rtbMsg.Name = "rtbMsg";
            this.rtbMsg.Size = new System.Drawing.Size(293, 176);
            this.rtbMsg.TabIndex = 4;
            this.rtbMsg.Text = "";
            // 
            // pnlSendMsgBtn
            // 
            this.pnlSendMsgBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlSendMsgBtn.BackgroundImage")));
            this.pnlSendMsgBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlSendMsgBtn.Location = new System.Drawing.Point(306, 205);
            this.pnlSendMsgBtn.Name = "pnlSendMsgBtn";
            this.pnlSendMsgBtn.Size = new System.Drawing.Size(33, 33);
            this.pnlSendMsgBtn.TabIndex = 0;
            this.pnlSendMsgBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlSendMsgBtn_MouseClick);
            // 
            // pnlLeaveRoomBtn
            // 
            this.pnlLeaveRoomBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlLeaveRoomBtn.BackgroundImage")));
            this.pnlLeaveRoomBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlLeaveRoomBtn.Location = new System.Drawing.Point(295, 3);
            this.pnlLeaveRoomBtn.Name = "pnlLeaveRoomBtn";
            this.pnlLeaveRoomBtn.Size = new System.Drawing.Size(33, 33);
            this.pnlLeaveRoomBtn.TabIndex = 3;
            // 
            // lblRoomName
            // 
            this.lblRoomName.AutoSize = true;
            this.lblRoomName.Location = new System.Drawing.Point(15, 10);
            this.lblRoomName.Name = "lblRoomName";
            this.lblRoomName.Size = new System.Drawing.Size(105, 13);
            this.lblRoomName.TabIndex = 1;
            this.lblRoomName.Text = "Название комнаты";
            // 
            // txtSendMsg
            // 
            this.txtSendMsg.Location = new System.Drawing.Point(3, 213);
            this.txtSendMsg.Name = "txtSendMsg";
            this.txtSendMsg.Size = new System.Drawing.Size(284, 20);
            this.txtSendMsg.TabIndex = 0;
            this.txtSendMsg.Text = "Отправить";
            this.txtSendMsg.Enter += new System.EventHandler(this.txtSendMsg_Enter);
            this.txtSendMsg.Leave += new System.EventHandler(this.txtSendMsg_Leave);
            // 
            // btnChangeLName
            // 
            this.btnChangeLName.Location = new System.Drawing.Point(381, 168);
            this.btnChangeLName.Name = "btnChangeLName";
            this.btnChangeLName.Size = new System.Drawing.Size(75, 23);
            this.btnChangeLName.TabIndex = 17;
            this.btnChangeLName.Text = "Изменить";
            this.btnChangeLName.UseVisualStyleBackColor = true;
            // 
            // txtLName
            // 
            this.txtLName.Location = new System.Drawing.Point(183, 171);
            this.txtLName.Name = "txtLName";
            this.txtLName.Size = new System.Drawing.Size(176, 20);
            this.txtLName.TabIndex = 16;
            // 
            // lblLName
            // 
            this.lblLName.AutoSize = true;
            this.lblLName.Location = new System.Drawing.Point(96, 178);
            this.lblLName.Name = "lblLName";
            this.lblLName.Size = new System.Drawing.Size(56, 13);
            this.lblLName.TabIndex = 15;
            this.lblLName.Text = "Фамилия";
            // 
            // btnChangeFName
            // 
            this.btnChangeFName.Location = new System.Drawing.Point(381, 142);
            this.btnChangeFName.Name = "btnChangeFName";
            this.btnChangeFName.Size = new System.Drawing.Size(75, 23);
            this.btnChangeFName.TabIndex = 14;
            this.btnChangeFName.Text = "Изменить";
            this.btnChangeFName.UseVisualStyleBackColor = true;
            // 
            // txtFName
            // 
            this.txtFName.Location = new System.Drawing.Point(183, 145);
            this.txtFName.Name = "txtFName";
            this.txtFName.Size = new System.Drawing.Size(176, 20);
            this.txtFName.TabIndex = 13;
            // 
            // lblFName
            // 
            this.lblFName.AutoSize = true;
            this.lblFName.Location = new System.Drawing.Point(96, 152);
            this.lblFName.Name = "lblFName";
            this.lblFName.Size = new System.Drawing.Size(29, 13);
            this.lblFName.TabIndex = 12;
            this.lblFName.Text = "Имя";
            // 
            // btnChangeLogin
            // 
            this.btnChangeLogin.Location = new System.Drawing.Point(381, 116);
            this.btnChangeLogin.Name = "btnChangeLogin";
            this.btnChangeLogin.Size = new System.Drawing.Size(75, 23);
            this.btnChangeLogin.TabIndex = 11;
            this.btnChangeLogin.Text = "Изменить";
            this.btnChangeLogin.UseVisualStyleBackColor = true;
            // 
            // txtLogin
            // 
            this.txtLogin.Location = new System.Drawing.Point(183, 119);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(176, 20);
            this.txtLogin.TabIndex = 10;
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Location = new System.Drawing.Point(96, 126);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(53, 13);
            this.lblLogin.TabIndex = 9;
            this.lblLogin.Text = "Никнейм";
            // 
            // btnChangePhone
            // 
            this.btnChangePhone.Location = new System.Drawing.Point(381, 90);
            this.btnChangePhone.Name = "btnChangePhone";
            this.btnChangePhone.Size = new System.Drawing.Size(75, 23);
            this.btnChangePhone.TabIndex = 8;
            this.btnChangePhone.Text = "Изменить";
            this.btnChangePhone.UseVisualStyleBackColor = true;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(183, 93);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(176, 20);
            this.txtPhone.TabIndex = 7;
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(96, 100);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(64, 13);
            this.lblPhone.TabIndex = 6;
            this.lblPhone.Text = "Номер тел.";
            // 
            // btnChangeMail
            // 
            this.btnChangeMail.Location = new System.Drawing.Point(381, 64);
            this.btnChangeMail.Name = "btnChangeMail";
            this.btnChangeMail.Size = new System.Drawing.Size(75, 23);
            this.btnChangeMail.TabIndex = 5;
            this.btnChangeMail.Text = "Изменить";
            this.btnChangeMail.UseVisualStyleBackColor = true;
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(183, 67);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(176, 20);
            this.txtMail.TabIndex = 4;
            // 
            // lblMail
            // 
            this.lblMail.AutoSize = true;
            this.lblMail.Location = new System.Drawing.Point(96, 74);
            this.lblMail.Name = "lblMail";
            this.lblMail.Size = new System.Drawing.Size(37, 13);
            this.lblMail.TabIndex = 3;
            this.lblMail.Text = "Почта";
            // 
            // btnChangePass
            // 
            this.btnChangePass.Location = new System.Drawing.Point(381, 38);
            this.btnChangePass.Name = "btnChangePass";
            this.btnChangePass.Size = new System.Drawing.Size(75, 23);
            this.btnChangePass.TabIndex = 2;
            this.btnChangePass.Text = "Изменить";
            this.btnChangePass.UseVisualStyleBackColor = true;
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(183, 41);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(176, 20);
            this.txtPass.TabIndex = 1;
            // 
            // lblPass
            // 
            this.lblPass.AutoSize = true;
            this.lblPass.Location = new System.Drawing.Point(96, 48);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(45, 13);
            this.lblPass.TabIndex = 0;
            this.lblPass.Text = "Пароль";
            // 
            // lblUserLogin
            // 
            this.lblUserLogin.AutoSize = true;
            this.lblUserLogin.BackColor = System.Drawing.Color.Transparent;
            this.lblUserLogin.ForeColor = System.Drawing.SystemColors.Control;
            this.lblUserLogin.Location = new System.Drawing.Point(115, 30);
            this.lblUserLogin.Name = "lblUserLogin";
            this.lblUserLogin.Size = new System.Drawing.Size(56, 13);
            this.lblUserLogin.TabIndex = 1;
            this.lblUserLogin.Text = "tempLogin";
            // 
            // pnlExitBtn
            // 
            this.pnlExitBtn.BackColor = System.Drawing.Color.Transparent;
            this.pnlExitBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlExitBtn.BackgroundImage")));
            this.pnlExitBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlExitBtn.Location = new System.Drawing.Point(178, 24);
            this.pnlExitBtn.Name = "pnlExitBtn";
            this.pnlExitBtn.Size = new System.Drawing.Size(23, 23);
            this.pnlExitBtn.TabIndex = 2;
            this.pnlExitBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlExitBtn_MouseClick);
            // 
            // pnlMyPage
            // 
            this.pnlMyPage.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlMyPage.BackColor = System.Drawing.Color.Transparent;
            this.pnlMyPage.Controls.Add(this.btnDnlPhoto);
            this.pnlMyPage.Controls.Add(this.lblPrivInf);
            this.pnlMyPage.Controls.Add(this.pnlPhoto);
            this.pnlMyPage.Controls.Add(this.pnlPrivInf);
            this.pnlMyPage.Location = new System.Drawing.Point(220, 2);
            this.pnlMyPage.Name = "pnlMyPage";
            this.pnlMyPage.Size = new System.Drawing.Size(521, 257);
            this.pnlMyPage.TabIndex = 3;
            // 
            // btnMyPage
            // 
            this.btnMyPage.Location = new System.Drawing.Point(111, 52);
            this.btnMyPage.Name = "btnMyPage";
            this.btnMyPage.Size = new System.Drawing.Size(99, 23);
            this.btnMyPage.TabIndex = 4;
            this.btnMyPage.Text = "Моя страница";
            this.btnMyPage.UseVisualStyleBackColor = true;
            this.btnMyPage.Click += new System.EventHandler(this.btnMyPage_Click);
            // 
            // btnMyDialogs
            // 
            this.btnMyDialogs.Location = new System.Drawing.Point(111, 74);
            this.btnMyDialogs.Name = "btnMyDialogs";
            this.btnMyDialogs.Size = new System.Drawing.Size(99, 23);
            this.btnMyDialogs.TabIndex = 5;
            this.btnMyDialogs.Text = "Мои диалоги";
            this.btnMyDialogs.UseVisualStyleBackColor = true;
            this.btnMyDialogs.Click += new System.EventHandler(this.btnMyDialogs_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.Location = new System.Drawing.Point(111, 96);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(99, 23);
            this.btnSettings.TabIndex = 6;
            this.btnSettings.Text = "Настройки";
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // pnlSettings
            // 
            this.pnlSettings.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlSettings.BackColor = System.Drawing.Color.Transparent;
            this.pnlSettings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSettings.Controls.Add(this.btnChangeLName);
            this.pnlSettings.Controls.Add(this.lblPass);
            this.pnlSettings.Controls.Add(this.txtLName);
            this.pnlSettings.Controls.Add(this.txtPass);
            this.pnlSettings.Controls.Add(this.lblLName);
            this.pnlSettings.Controls.Add(this.btnChangePass);
            this.pnlSettings.Controls.Add(this.btnChangeFName);
            this.pnlSettings.Controls.Add(this.lblMail);
            this.pnlSettings.Controls.Add(this.txtFName);
            this.pnlSettings.Controls.Add(this.txtMail);
            this.pnlSettings.Controls.Add(this.lblFName);
            this.pnlSettings.Controls.Add(this.btnChangeMail);
            this.pnlSettings.Controls.Add(this.btnChangeLogin);
            this.pnlSettings.Controls.Add(this.lblPhone);
            this.pnlSettings.Controls.Add(this.txtLogin);
            this.pnlSettings.Controls.Add(this.txtPhone);
            this.pnlSettings.Controls.Add(this.lblLogin);
            this.pnlSettings.Controls.Add(this.btnChangePhone);
            this.pnlSettings.Location = new System.Drawing.Point(210, 21);
            this.pnlSettings.Name = "pnlSettings";
            this.pnlSettings.Size = new System.Drawing.Size(548, 407);
            this.pnlSettings.TabIndex = 7;
            this.pnlSettings.Visible = false;
            // 
            // pnlMyDialogs
            // 
            this.pnlMyDialogs.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlMyDialogs.BackColor = System.Drawing.Color.Transparent;
            this.pnlMyDialogs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMyDialogs.Controls.Add(this.pnlUsers);
            this.pnlMyDialogs.Controls.Add(this.pnlRoomMsg);
            this.pnlMyDialogs.Controls.Add(this.pnlRooms);
            this.pnlMyDialogs.Location = new System.Drawing.Point(210, 21);
            this.pnlMyDialogs.Name = "pnlMyDialogs";
            this.pnlMyDialogs.Size = new System.Drawing.Size(548, 407);
            this.pnlMyDialogs.TabIndex = 7;
            this.pnlMyDialogs.Visible = false;
            // 
            // pnlCreateRoom
            // 
            this.pnlCreateRoom.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlCreateRoom.BackColor = System.Drawing.Color.Transparent;
            this.pnlCreateRoom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCreateRoom.Controls.Add(this.pnlCreateRoomBtn);
            this.pnlCreateRoom.Controls.Add(this.txtNewRoomName);
            this.pnlCreateRoom.Location = new System.Drawing.Point(209, 20);
            this.pnlCreateRoom.Name = "pnlCreateRoom";
            this.pnlCreateRoom.Size = new System.Drawing.Size(548, 407);
            this.pnlCreateRoom.TabIndex = 8;
            this.pnlCreateRoom.Visible = false;
            // 
            // pnlCreateRoomBtn
            // 
            this.pnlCreateRoomBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlCreateRoomBtn.BackgroundImage")));
            this.pnlCreateRoomBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlCreateRoomBtn.Location = new System.Drawing.Point(483, 19);
            this.pnlCreateRoomBtn.Name = "pnlCreateRoomBtn";
            this.pnlCreateRoomBtn.Size = new System.Drawing.Size(33, 33);
            this.pnlCreateRoomBtn.TabIndex = 4;
            this.pnlCreateRoomBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlCreateRoomBtn_MouseClick);
            // 
            // txtNewRoomName
            // 
            this.txtNewRoomName.Location = new System.Drawing.Point(42, 26);
            this.txtNewRoomName.Name = "txtNewRoomName";
            this.txtNewRoomName.Size = new System.Drawing.Size(415, 20);
            this.txtNewRoomName.TabIndex = 0;
            this.txtNewRoomName.Text = "Введите название беседы";
            this.txtNewRoomName.Enter += new System.EventHandler(this.txtNewRoomName_Enter);
            this.txtNewRoomName.Leave += new System.EventHandler(this.txtNewRoomName_Leave);
            // 
            // UserMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(817, 465);
            this.Controls.Add(this.pnlCreateRoom);
            this.Controls.Add(this.pnlMyDialogs);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.pnlSettings);
            this.Controls.Add(this.btnMyDialogs);
            this.Controls.Add(this.btnMyPage);
            this.Controls.Add(this.pnlMyPage);
            this.Controls.Add(this.pnlExitBtn);
            this.Controls.Add(this.lblUserLogin);
            this.DoubleBuffered = true;
            this.Name = "UserMainForm";
            this.Text = "Chat";
            this.Load += new System.EventHandler(this.UserMainForm_Load);
            this.pnlPrivInf.ResumeLayout(false);
            this.pnlPrivInf.PerformLayout();
            this.pnlUsers.ResumeLayout(false);
            this.pnlUsers.PerformLayout();
            this.pnlRooms.ResumeLayout(false);
            this.pnlRooms.PerformLayout();
            this.pnlRoomMsg.ResumeLayout(false);
            this.pnlRoomMsg.PerformLayout();
            this.pnlMyPage.ResumeLayout(false);
            this.pnlMyPage.PerformLayout();
            this.pnlSettings.ResumeLayout(false);
            this.pnlSettings.PerformLayout();
            this.pnlMyDialogs.ResumeLayout(false);
            this.pnlCreateRoom.ResumeLayout(false);
            this.pnlCreateRoom.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnChangeLName;
        private System.Windows.Forms.TextBox txtLName;
        private System.Windows.Forms.Label lblLName;
        private System.Windows.Forms.Button btnChangeFName;
        private System.Windows.Forms.TextBox txtFName;
        private System.Windows.Forms.Label lblFName;
        private System.Windows.Forms.Button btnChangeLogin;
        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Button btnChangePhone;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Button btnChangeMail;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.Label lblMail;
        private System.Windows.Forms.Button btnChangePass;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.Label lblUserLogin;
        private System.Windows.Forms.Panel pnlExitBtn;
        private System.Windows.Forms.Label lblPrivInf;
        private System.Windows.Forms.Panel pnlPrivInf;
        private System.Windows.Forms.Label lblMailInfo;
        private System.Windows.Forms.Label lblPhoneInfo;
        private System.Windows.Forms.Label lblLNameInfo;
        private System.Windows.Forms.Label lblFnameInfo;
        private System.Windows.Forms.Button btnDnlPhoto;
        private System.Windows.Forms.Panel pnlPhoto;
        private System.Windows.Forms.Panel pnlUsers;
        private System.Windows.Forms.Panel pnlRooms;
        private System.Windows.Forms.Panel pnlRoomMsg;
        private System.Windows.Forms.Panel pnlLeaveRoomBtn;
        private System.Windows.Forms.Panel pnlSendMsgBtn;
        private System.Windows.Forms.Label lblRoomName;
        private System.Windows.Forms.TextBox txtSendMsg;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label lblRooms;
        private System.Windows.Forms.Label lblUserList;
        private System.Windows.Forms.Panel pnlComplainBtn;
        private System.Windows.Forms.Panel pnlAddRoomBtn;
        private System.Windows.Forms.Panel pnlMyPage;
        private System.Windows.Forms.Button btnMyPage;
        private System.Windows.Forms.Panel pnlSettings;
        private System.Windows.Forms.Button btnMyDialogs;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Panel pnlMyDialogs;
        public System.Windows.Forms.ListView lvUsers;
        public System.Windows.Forms.ListView lvRooms;
        public  System.Windows.Forms.RichTextBox rtbMsg;
        private System.Windows.Forms.Panel pnlCreateRoom;
        private System.Windows.Forms.TextBox txtNewRoomName;
        private System.Windows.Forms.Panel pnlCreateRoomBtn;
        private System.Windows.Forms.ImageList imageList;
    }
}