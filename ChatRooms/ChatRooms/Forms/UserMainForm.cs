﻿using ChatModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatRooms
{
    public partial class UserMainForm : Form
    {
        //ChatEngine engine = new ChatEngine();
        Form parrent;
        Room currentRoom;
        ChatEngine engine = Program.engine;
        List<string> allRoomsNames = new List<string>();
        string[] txtFields = { "Поиск", "Отправить", "Введите название беседы"};

        public UserMainForm(Form parrent)
        {
            InitializeComponent();
            this.parrent = parrent;
            engine.InitMessageModule(this);
            engine.InitRoomMenagerModule();
            lblFnameInfo.Text = Program.chatClient.User.FirstName;
            lblLNameInfo.Text = Program.chatClient.User.LastName;
            lblMailInfo.Text = Program.chatClient.User.Email;
            lblPhoneInfo.Text = Program.chatClient.User.Phone;
            lblUserLogin.Text = Program.chatClient.User.Login;
           

        }
        private void btnMyPage_Click(object sender, EventArgs e)
        {
            engine.StartMessaging();

            pnlMyPage.Visible = true;
            pnlMyDialogs.Visible = false;
            pnlSettings.Visible = false;
            pnlCreateRoom.Visible = false;
            pnlExitBtn.Visible = true;
            pnlExitBtn.Enabled = true;
        }

        private void btnMyDialogs_Click(object sender, EventArgs e)
        {
            engine.StartMessaging();
            engine.GetRooms();


            pnlMyPage.Visible = false;
            pnlSettings.Visible = false;
            pnlMyDialogs.Visible = true;
            pnlCreateRoom.Visible = false;
            pnlExitBtn.Visible = false;
            pnlExitBtn.Enabled = false;
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            engine.StopMessaging();

            pnlSettings.Visible = true;
            pnlMyDialogs.Visible = false;
            pnlMyPage.Visible = false;
            pnlExitBtn.Visible = false;
            pnlCreateRoom.Visible = false;
            pnlExitBtn.Enabled = false;
        }

        private void UserMainForm_Load(object sender, EventArgs e)
        {
            pnlMyPage.Visible = true;
            pnlMyDialogs.Visible = false;
            pnlSettings.Visible = false;
            pnlCreateRoom.Visible = false;
        }
        private void pnlAddRoomBtn_MouseClick(object sender, MouseEventArgs e)
        {
            pnlMyDialogs.Visible = false;
            pnlCreateRoom.Visible = true;
        }
        private void pnlCreateRoomBtn_MouseClick(object sender, MouseEventArgs e)
        {
            Room newRoom = new Room(txtNewRoomName.Text);
            engine.CreateRoom(newRoom);
            pnlCreateRoom.Visible = false;
            pnlMyDialogs.Visible = true;
        }
        private void pnlExitBtn_MouseClick(object sender, MouseEventArgs e)
        {


        }

        private void lvRooms_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView lv = (ListView)sender;
            //
            //ColumnHeader header = new ColumnHeader();
            //header.Text = "";
            //header.Name = "col1";
            //lv.Columns.Add(header);

            //
            for (int i = 0; i < lv.Items.Count; i++)
            {
                lv.Items[i].ImageIndex = 1;
            }
            if (lv.SelectedItems.Count > 0)
            {
                lv.SelectedItems[0].ImageIndex = 0;
            }
            //


            ListViewItem item;
            if (lv.SelectedItems.Count > 0)
            {
                item = lv.SelectedItems[0];
                if(currentRoom == null)
                {
                    currentRoom = new Room(item.Text);
                    engine.EnterInRoom(currentRoom);
                }
                else
                {
                    engine.LeaveRoom(currentRoom);
                    currentRoom = new Room(item.Text);
                    engine.EnterInRoom(currentRoom);
                }
               
            }
            
            //msgModule.GetInitMsgs(new Room(lv.SelectedItems[0].Name));
        }

        private void pnlSendMsgBtn_MouseClick(object sender, MouseEventArgs e)
        {
            engine.SendMessage(txtSendMsg.Text, currentRoom);
            txtSendMsg.Text = "";
        }

        private void lvRooms_ItemActivate(object sender, EventArgs e)
        {

        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            TextBoxEnter((TextBox)sender, txtFields[0]);
        }

        private void txtSearch_Leave(object sender, EventArgs e)
        {
            TextBoxLeave((TextBox)sender, txtFields[0]);
        }
        private void txtSendMsg_Enter(object sender, EventArgs e)
        {
            TextBoxEnter((TextBox)sender, txtFields[1]);
        }

        private void txtSendMsg_Leave(object sender, EventArgs e)
        {
            TextBoxLeave((TextBox)sender, txtFields[1]);
        }
        private void txtNewRoomName_Enter(object sender, EventArgs e)
        {
            TextBoxEnter((TextBox)sender, txtFields[2]);
        }

        private void txtNewRoomName_Leave(object sender, EventArgs e)
        {
            TextBoxLeave((TextBox)sender, txtFields[2]);
        }



        private void TextBoxEnter(TextBox txtBox, string str)
        {
            if (txtBox.Text == str)
                txtBox.Text = "";
        }
        private void TextBoxLeave(TextBox txtBox, string str)
        {
            if (txtBox.Text == "")
                txtBox.Text = str;
        }
        ///

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
        }
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text == "" || txtSearch.Text == "Поиск")
            {
                if (allRoomsNames.Count == 0)
                {
                    for (int i = 0; i < lvRooms.Items.Count; i++)
                    {
                        allRoomsNames.Add(lvRooms.Items[i].Text);
                    }
                }
                else
                {
                    lvRooms.Clear();
                    for (int i = 0; i < allRoomsNames.Count; i++)
                    {
                        lvRooms.Items.Add(new ListViewItem(allRoomsNames[i]));
                    }
                }
            }
            else
            {
                lvRooms.Clear();
                for (int i = 0; i < allRoomsNames.Count; i++)
                {
                    if (allRoomsNames[i].Contains(txtSearch.Text))
                        lvRooms.Items.Add(new ListViewItem(allRoomsNames[i]));
                }
            }
        }

        private void pnlCreateRoomBtn_Paint(object sender, PaintEventArgs e)
        {
            Room newRoom = new Room(txtNewRoomName.Text);
            engine.CreateRoom(newRoom);
        }


    }
}
