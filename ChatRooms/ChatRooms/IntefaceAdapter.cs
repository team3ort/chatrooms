﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChatModels;

namespace ChatRooms
{
    public class IntefaceAdapter
    {

        UserMainForm form;

        public IntefaceAdapter (UserMainForm form)
        {
            this.form = form;
        }

        public void AppendTextSafe(string newText)
        {
            if (form.rtbMsg.InvokeRequired) form.rtbMsg.Invoke(new Action<string>((s) => form.rtbMsg.AppendText(s)), newText);
            else form.rtbMsg.AppendText(newText);
        }

        public void ClearMessageSafe()
        {
            if (form.lvRooms.InvokeRequired) form.rtbMsg.Invoke(new Action(() => form.rtbMsg.Text = ""));
            else form.lvRooms.Items.Clear();
        }

        public void AddRoomsSafe(List<Room> rooms)
        {
            for(int i = 0; i < rooms.Count; i++)
            {
                ListViewItem item = new ListViewItem(rooms[i].Name);
                if (form.lvRooms.InvokeRequired) form.lvRooms.Invoke(new Action<ListViewItem>((s) => form.lvRooms.Items.Add(s)), item);
                else form.lvRooms.Items.Add(item);
            }           
        }

        public void ClearRoomsSafe()
        {
            if (form.lvRooms.InvokeRequired) form.lvRooms.Invoke(new Action(() => form.lvRooms.Items.Clear()));
            else form.lvRooms.Items.Clear();
        }
        public void ClearUsersSafe()
        {
            if (form.lvUsers.InvokeRequired) form.lvUsers.Invoke(new Action(() => form.lvUsers.Items.Clear()));
            else form.lvUsers.Items.Clear();
        }
        public void AddUsersSafe(List<User> users)
        {
            for (int i = 0; i < users.Count; i++)
            {
                ListViewItem item = new ListViewItem(users[i].Login);
                if (form.lvUsers.InvokeRequired) form.lvUsers.Invoke(new Action<ListViewItem>((s) => form.lvUsers.Items.Add(s)), item);
                else form.lvUsers.Items.Add(item);
            }
            
        }
    }
}
