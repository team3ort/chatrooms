﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatRooms
{
    static class Program
    {        /// <summary>
             /// The main entry point for the application.
             /// </summary>
             /// 
             ///http://sealaunch.ddns.net:8080/
        //
        public static TcpConnection connection = new TcpConnection();
        public static ChatClient chatClient;
        public static ChatEngine engine = new ChatEngine();
        [STAThread]
        static void Main()
        {
            chatClient = new ChatClient("192.168.4.112", 11155);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AuthRegForm());
        }

        
    }
}
