﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatModels
{
    public class Message
    {
        public Message()
        {

        }
        public Message(User author, Room room, string msgdata)
        {
            Author = author;
            Room = room;
            CreationDate = DateTime.Now;
            MsgData = msgdata;

        }

        public User Author { get; set; }
        public Room Room { get; set; }
        public DateTime CreationDate { get; set; }
        public string MsgData { get; set; }

    }
}
