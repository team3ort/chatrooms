﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatModels
{
    public class ResponsePackage
    {
        public string DataType { get; set; }
        public string Data { get; set; }

        public ResponsePackage()
        {

        }

        public ResponsePackage(string dataType, string data)
        {
            DataType = dataType;
            Data = data;
        }

    }
}
