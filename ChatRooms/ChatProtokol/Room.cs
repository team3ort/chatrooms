﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatModels
{
    public class Room
    {
        public Room(string name)
        {
            Name = name;
            UsersInRoom = new List<User>();
        }


        public int Id { get; set; }
        public string Name { get; set; }
        public User Creator { get; set; }
        public List<User> UsersInRoom { get; set; }

        public override bool Equals(object obj)
        {
            if (this == null && obj == null)
                return true;
            if (obj.GetType() == typeof(Room))
            {
                Room oRoom = (Room)obj;
                if (this.Name == oRoom.Name)
                    return true;
                else
                    return false;
            }
            else return false;
        }
    }
}
