﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatModels
{
    public class User
    {
        public User()
        {

        }

        public User(String login)
        {
            Login = login;
        }
        public User(string login, string password, string firstName, string lastName, string email, string phone)
        {
            Login = login;
            Password = password;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Phone = phone;
        }

        public User getName()
        {
            return new User(Login);
        }

        public User(string login, string password)
        {
            Login = login;
            Password = password;
        }

        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsAdmin { get; set; }
        public string ImagePhotoPath { get; set; }

        List<Room> ConnectedRooms { get; set; }

        public override bool Equals(object obj)
        {
            if (this == null && obj == null)
                return true;
            if (obj.GetType() == typeof(User))
            {
                User oUser = (User)obj;
                if (this.Login == oUser.Login || this.Email == oUser.Email)
                    return true;
                else
                    return false;
            }
            else return false;
            
        }

        public User Clone()
        {
            User result = new User();
            result.Email = this.Email;
            result.FirstName = this.FirstName;
            result.Id = this.Id;
            result.ImagePhotoPath = this.ImagePhotoPath;
            result.IsAdmin = this.IsAdmin;
            result.LastName = this.LastName;
            result.Login = this.Login;
            result.Password = this.Password;
            result.Phone = this.Phone;
            return result;
        }
    }
}
