﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatModels
{
    public class Error
    {
        public string Message { get; set; }

        public Error()
        {

        }
        public Error(string message)
        {
            Message = message;
        }
    }
}
