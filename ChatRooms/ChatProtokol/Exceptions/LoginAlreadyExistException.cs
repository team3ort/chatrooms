﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatModels.Exceptions
{
    public class LoginAlreadyExistException : Exception
    {
        public LoginAlreadyExistException()
        {
        }

        public LoginAlreadyExistException(string message) : base(message)
        {
        }
    }
}
