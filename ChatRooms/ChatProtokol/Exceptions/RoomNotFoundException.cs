﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatModels.Exceptions
{
    public class RoomNotFoundException : Exception
    {
        public RoomNotFoundException()
        {

        }

        public RoomNotFoundException(string message) : base(message)
        {

        }
    }
}
