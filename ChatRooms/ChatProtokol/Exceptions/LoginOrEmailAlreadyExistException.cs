﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatModels.Exceptions
{
    public class LoginOrEmailAlreadyExistException : Exception
    {
        public LoginOrEmailAlreadyExistException()
        {

        }

        public LoginOrEmailAlreadyExistException(string message) : base(message)
        {

        }
    }
}
