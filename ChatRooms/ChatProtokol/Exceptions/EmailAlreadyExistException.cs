﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatModels.Exceptions
{
    public class EmailAlreadyExistException : Exception
    {
        public EmailAlreadyExistException()
        {
        }

        public EmailAlreadyExistException(string message) : base(message)
        {

        }
    }
}
