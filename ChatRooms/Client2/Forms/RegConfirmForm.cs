﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatRooms
{
    public partial class RegConfirmForm : Form
    {
        Form parrent;
        public RegConfirmForm(Form parrent)
        {
            this.parrent = parrent;
            InitializeComponent();
        }

        private void btnGoHome_Click(object sender, EventArgs e)
        {
            parrent.Show();
            this.Close();
        }
    }
}
