﻿namespace ChatRooms
{
    partial class AuthRegForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthRegForm));
            this.lblAuth = new System.Windows.Forms.Label();
            this.pnlAuth = new System.Windows.Forms.Panel();
            this.pnlLoginBtn = new System.Windows.Forms.Panel();
            this.pnlPassImg = new System.Windows.Forms.Panel();
            this.pnlLogImg = new System.Windows.Forms.Panel();
            this.lblForgotPassBtn = new System.Windows.Forms.Label();
            this.txtAuthPassword = new System.Windows.Forms.TextBox();
            this.txtAuthLogin = new System.Windows.Forms.TextBox();
            this.lblReg = new System.Windows.Forms.Label();
            this.pnlReg = new System.Windows.Forms.Panel();
            this.pnlRegBtn = new System.Windows.Forms.Panel();
            this.lblRegBtn = new System.Windows.Forms.Label();
            this.pnlRegImg = new System.Windows.Forms.Panel();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.lblFastReg = new System.Windows.Forms.Label();
            this.pnlAuth.SuspendLayout();
            this.pnlReg.SuspendLayout();
            this.pnlRegBtn.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblAuth
            // 
            this.lblAuth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAuth.AutoSize = true;
            this.lblAuth.BackColor = System.Drawing.Color.Transparent;
            this.lblAuth.Location = new System.Drawing.Point(753, 11);
            this.lblAuth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAuth.Name = "lblAuth";
            this.lblAuth.Size = new System.Drawing.Size(77, 17);
            this.lblAuth.TabIndex = 0;
            this.lblAuth.Text = "Вход в чат";
            // 
            // pnlAuth
            // 
            this.pnlAuth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlAuth.BackColor = System.Drawing.Color.Transparent;
            this.pnlAuth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAuth.Controls.Add(this.pnlLoginBtn);
            this.pnlAuth.Controls.Add(this.pnlPassImg);
            this.pnlAuth.Controls.Add(this.pnlLogImg);
            this.pnlAuth.Controls.Add(this.lblForgotPassBtn);
            this.pnlAuth.Controls.Add(this.txtAuthPassword);
            this.pnlAuth.Controls.Add(this.txtAuthLogin);
            this.pnlAuth.Location = new System.Drawing.Point(757, 36);
            this.pnlAuth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlAuth.Name = "pnlAuth";
            this.pnlAuth.Size = new System.Drawing.Size(315, 161);
            this.pnlAuth.TabIndex = 1;
            // 
            // pnlLoginBtn
            // 
            this.pnlLoginBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlLoginBtn.BackColor = System.Drawing.Color.Transparent;
            this.pnlLoginBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlLoginBtn.BackgroundImage")));
            this.pnlLoginBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlLoginBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlLoginBtn.Location = new System.Drawing.Point(243, 94);
            this.pnlLoginBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlLoginBtn.Name = "pnlLoginBtn";
            this.pnlLoginBtn.Size = new System.Drawing.Size(64, 53);
            this.pnlLoginBtn.TabIndex = 0;
            this.pnlLoginBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlLoginBtn_MouseClick);
            // 
            // pnlPassImg
            // 
            this.pnlPassImg.BackColor = System.Drawing.Color.Transparent;
            this.pnlPassImg.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlPassImg.BackgroundImage")));
            this.pnlPassImg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlPassImg.Location = new System.Drawing.Point(51, 53);
            this.pnlPassImg.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlPassImg.Name = "pnlPassImg";
            this.pnlPassImg.Size = new System.Drawing.Size(41, 38);
            this.pnlPassImg.TabIndex = 4;
            // 
            // pnlLogImg
            // 
            this.pnlLogImg.BackColor = System.Drawing.Color.Transparent;
            this.pnlLogImg.BackgroundImage = global::ChatRooms.Properties.Resources._4;
            this.pnlLogImg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlLogImg.Location = new System.Drawing.Point(49, 7);
            this.pnlLogImg.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlLogImg.Name = "pnlLogImg";
            this.pnlLogImg.Size = new System.Drawing.Size(41, 38);
            this.pnlLogImg.TabIndex = 4;
            // 
            // lblForgotPassBtn
            // 
            this.lblForgotPassBtn.AutoSize = true;
            this.lblForgotPassBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblForgotPassBtn.Location = new System.Drawing.Point(116, 117);
            this.lblForgotPassBtn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblForgotPassBtn.Name = "lblForgotPassBtn";
            this.lblForgotPassBtn.Size = new System.Drawing.Size(118, 17);
            this.lblForgotPassBtn.TabIndex = 2;
            this.lblForgotPassBtn.Text = "Забыли пароль?";
            this.lblForgotPassBtn.Click += new System.EventHandler(this.lblForgotPassBtn_Click);
            // 
            // txtAuthPassword
            // 
            this.txtAuthPassword.Location = new System.Drawing.Point(111, 64);
            this.txtAuthPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtAuthPassword.Name = "txtAuthPassword";
            this.txtAuthPassword.PasswordChar = '*';
            this.txtAuthPassword.Size = new System.Drawing.Size(197, 22);
            this.txtAuthPassword.TabIndex = 1;
            // 
            // txtAuthLogin
            // 
            this.txtAuthLogin.Location = new System.Drawing.Point(111, 20);
            this.txtAuthLogin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtAuthLogin.Name = "txtAuthLogin";
            this.txtAuthLogin.Size = new System.Drawing.Size(197, 22);
            this.txtAuthLogin.TabIndex = 0;
            // 
            // lblReg
            // 
            this.lblReg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblReg.AutoSize = true;
            this.lblReg.BackColor = System.Drawing.Color.Transparent;
            this.lblReg.Location = new System.Drawing.Point(753, 220);
            this.lblReg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblReg.Name = "lblReg";
            this.lblReg.Size = new System.Drawing.Size(92, 17);
            this.lblReg.TabIndex = 2;
            this.lblReg.Text = "Регистрация";
            // 
            // pnlReg
            // 
            this.pnlReg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlReg.BackColor = System.Drawing.Color.Transparent;
            this.pnlReg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlReg.Controls.Add(this.pnlRegBtn);
            this.pnlReg.Controls.Add(this.txtPass);
            this.pnlReg.Controls.Add(this.txtPhone);
            this.pnlReg.Controls.Add(this.txtEmail);
            this.pnlReg.Controls.Add(this.txtLogin);
            this.pnlReg.Controls.Add(this.txtLastName);
            this.pnlReg.Controls.Add(this.txtFirstName);
            this.pnlReg.Controls.Add(this.lblFastReg);
            this.pnlReg.Location = new System.Drawing.Point(757, 245);
            this.pnlReg.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlReg.Name = "pnlReg";
            this.pnlReg.Size = new System.Drawing.Size(315, 312);
            this.pnlReg.TabIndex = 3;
            // 
            // pnlRegBtn
            // 
            this.pnlRegBtn.BackColor = System.Drawing.Color.Transparent;
            this.pnlRegBtn.Controls.Add(this.lblRegBtn);
            this.pnlRegBtn.Controls.Add(this.pnlRegImg);
            this.pnlRegBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlRegBtn.Location = new System.Drawing.Point(43, 249);
            this.pnlRegBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlRegBtn.Name = "pnlRegBtn";
            this.pnlRegBtn.Size = new System.Drawing.Size(237, 46);
            this.pnlRegBtn.TabIndex = 6;
            this.pnlRegBtn.Click += new System.EventHandler(this.Register_Click);
            // 
            // lblRegBtn
            // 
            this.lblRegBtn.AutoSize = true;
            this.lblRegBtn.BackColor = System.Drawing.Color.Transparent;
            this.lblRegBtn.Location = new System.Drawing.Point(73, 16);
            this.lblRegBtn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRegBtn.Name = "lblRegBtn";
            this.lblRegBtn.Size = new System.Drawing.Size(144, 17);
            this.lblRegBtn.TabIndex = 4;
            this.lblRegBtn.Text = "Зарегистрироваться";
            this.lblRegBtn.Click += new System.EventHandler(this.Register_Click);
            // 
            // pnlRegImg
            // 
            this.pnlRegImg.BackColor = System.Drawing.Color.Transparent;
            this.pnlRegImg.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlRegImg.BackgroundImage")));
            this.pnlRegImg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlRegImg.Location = new System.Drawing.Point(7, 7);
            this.pnlRegImg.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlRegImg.Name = "pnlRegImg";
            this.pnlRegImg.Size = new System.Drawing.Size(41, 38);
            this.pnlRegImg.TabIndex = 4;
            this.pnlRegImg.Click += new System.EventHandler(this.Register_Click);
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(43, 199);
            this.txtPass.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(235, 22);
            this.txtPass.TabIndex = 7;
            this.txtPass.Text = "Password";
            this.txtPass.Enter += new System.EventHandler(this.txtPass_Enter);
            this.txtPass.Leave += new System.EventHandler(this.txtPass_Leave);
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(43, 166);
            this.txtPhone.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(236, 22);
            this.txtPhone.TabIndex = 5;
            this.txtPhone.Text = "PhoneNumber";
            this.txtPhone.Enter += new System.EventHandler(this.txtPhone_Enter);
            this.txtPhone.Leave += new System.EventHandler(this.txtPhone_Leave);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(43, 133);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(236, 22);
            this.txtEmail.TabIndex = 4;
            this.txtEmail.Text = "Mail";
            this.txtEmail.Enter += new System.EventHandler(this.txtEmail_Enter);
            this.txtEmail.Leave += new System.EventHandler(this.txtEmail_Leave);
            // 
            // txtLogin
            // 
            this.txtLogin.Location = new System.Drawing.Point(43, 100);
            this.txtLogin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(236, 22);
            this.txtLogin.TabIndex = 3;
            this.txtLogin.Text = "NickName";
            this.txtLogin.Enter += new System.EventHandler(this.txtLogin_Enter);
            this.txtLogin.Leave += new System.EventHandler(this.txtLogin_Leave);
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(43, 66);
            this.txtLastName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(236, 22);
            this.txtLastName.TabIndex = 2;
            this.txtLastName.Text = "LastName";
            this.txtLastName.Enter += new System.EventHandler(this.txtLastName_Enter);
            this.txtLastName.Leave += new System.EventHandler(this.txtLastName_Leave);
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(43, 33);
            this.txtFirstName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(236, 22);
            this.txtFirstName.TabIndex = 1;
            this.txtFirstName.Text = "FirstName";
            this.txtFirstName.Enter += new System.EventHandler(this.txtFirstName_Enter);
            this.txtFirstName.Leave += new System.EventHandler(this.txtFirstName_Leave);
            // 
            // lblFastReg
            // 
            this.lblFastReg.AutoSize = true;
            this.lblFastReg.Location = new System.Drawing.Point(39, 12);
            this.lblFastReg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFastReg.Name = "lblFastReg";
            this.lblFastReg.Size = new System.Drawing.Size(152, 17);
            this.lblFastReg.TabIndex = 0;
            this.lblFastReg.Text = "Быстрая регистрация";
            // 
            // AuthRegForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1089, 572);
            this.Controls.Add(this.pnlReg);
            this.Controls.Add(this.lblReg);
            this.Controls.Add(this.pnlAuth);
            this.Controls.Add(this.lblAuth);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "AuthRegForm";
            this.Text = "Chat";
            this.pnlAuth.ResumeLayout(false);
            this.pnlAuth.PerformLayout();
            this.pnlReg.ResumeLayout(false);
            this.pnlReg.PerformLayout();
            this.pnlRegBtn.ResumeLayout(false);
            this.pnlRegBtn.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAuth;
        private System.Windows.Forms.Panel pnlAuth;
        private System.Windows.Forms.Panel pnlLoginBtn;
        private System.Windows.Forms.TextBox txtAuthLogin;
        private System.Windows.Forms.TextBox txtAuthPassword;
        private System.Windows.Forms.Label lblForgotPassBtn;
        private System.Windows.Forms.Label lblReg;
        private System.Windows.Forms.Panel pnlReg;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label lblFastReg;
        private System.Windows.Forms.Panel pnlLogImg;
        private System.Windows.Forms.Panel pnlPassImg;
        private System.Windows.Forms.Panel pnlRegBtn;
        private System.Windows.Forms.Label lblRegBtn;
        private System.Windows.Forms.Panel pnlRegImg;
        private System.Windows.Forms.TextBox txtPass;
    }
}

