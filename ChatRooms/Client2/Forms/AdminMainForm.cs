﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatRooms
{
    public partial class AdminMainForm : Form
    {
        public AdminMainForm()
        {
            InitializeComponent();
        }
        private void btnMyPage_Click(object sender, EventArgs e)
        {
            pnlMyPage.Visible = true;
            pnlMyDialogs.Visible = false;
            pnlRequests.Visible = false;
            pnlCreateRoom.Visible = false;
            pnlExitBtn.Visible = true;
            pnlExitBtn.Enabled = true;
        }
        private void btnMyDialogs_Click_1(object sender, EventArgs e)
        {
            pnlMyPage.Visible = false;
            pnlRequests.Visible = false;
            pnlMyDialogs.Visible = true;
            pnlCreateRoom.Visible = false;
            pnlExitBtn.Visible = false;
            pnlExitBtn.Enabled = false;
        }

        private void btnRequests_Click(object sender, EventArgs e)
        {
            pnlRequests.Visible = true;
            pnlMyDialogs.Visible = false;
            pnlMyPage.Visible = false;
            pnlCreateRoom.Visible = false;
            pnlExitBtn.Visible = false;
            pnlExitBtn.Enabled = false;
        }

        private void AdminMainForm_Load(object sender, EventArgs e)
        {
            pnlMyPage.Visible = true;
            pnlMyDialogs.Visible = false;
            pnlRequests.Visible = false;
            pnlCreateRoom.Visible = false;
        }

        private void pnlAddRoomBtn_MouseClick(object sender, MouseEventArgs e)
        {
            pnlMyDialogs.Visible = false;
            pnlCreateRoom.Visible = true;
        }

        private void pnlCreateRoomBtn_MouseClick(object sender, MouseEventArgs e)
        {
            pnlCreateRoom.Visible = false;
            pnlMyDialogs.Visible = true;
        }

        private void pnlExitBtn_MouseClick(object sender, MouseEventArgs e)
        {
            AuthRegForm ARForm = new AuthRegForm();
            ARForm.Show();
            Hide();
        }
    }
}
