﻿namespace ChatRooms
{
    partial class RegConfirmForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegConfirmForm));
            this.pnlRegConfirm = new System.Windows.Forms.Panel();
            this.lblRegConirm = new System.Windows.Forms.Label();
            this.btnGoHome = new System.Windows.Forms.Button();
            this.pnlRegConfirm.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlRegConfirm
            // 
            this.pnlRegConfirm.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlRegConfirm.BackColor = System.Drawing.Color.Transparent;
            this.pnlRegConfirm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRegConfirm.Controls.Add(this.lblRegConirm);
            this.pnlRegConfirm.Location = new System.Drawing.Point(235, 33);
            this.pnlRegConfirm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlRegConfirm.Name = "pnlRegConfirm";
            this.pnlRegConfirm.Size = new System.Drawing.Size(623, 96);
            this.pnlRegConfirm.TabIndex = 0;
            // 
            // lblRegConirm
            // 
            this.lblRegConirm.AutoSize = true;
            this.lblRegConirm.BackColor = System.Drawing.Color.Transparent;
            this.lblRegConirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRegConirm.Location = new System.Drawing.Point(87, 17);
            this.lblRegConirm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRegConirm.Name = "lblRegConirm";
            this.lblRegConirm.Size = new System.Drawing.Size(431, 50);
            this.lblRegConirm.TabIndex = 1;
            this.lblRegConirm.Text = "Добро пожаловать! Регистрация прошла\r\nуспешно, можете перейти на свою страницу";
            this.lblRegConirm.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnGoHome
            // 
            this.btnGoHome.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnGoHome.Location = new System.Drawing.Point(423, 150);
            this.btnGoHome.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGoHome.Name = "btnGoHome";
            this.btnGoHome.Size = new System.Drawing.Size(251, 28);
            this.btnGoHome.TabIndex = 1;
            this.btnGoHome.Text = "Перейти на стартовую страницу";
            this.btnGoHome.UseVisualStyleBackColor = true;
            this.btnGoHome.Click += new System.EventHandler(this.btnGoHome_Click);
            // 
            // RegConfirmForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1089, 572);
            this.Controls.Add(this.btnGoHome);
            this.Controls.Add(this.pnlRegConfirm);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "RegConfirmForm";
            this.Text = "Chat";
            this.pnlRegConfirm.ResumeLayout(false);
            this.pnlRegConfirm.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlRegConfirm;
        private System.Windows.Forms.Label lblRegConirm;
        private System.Windows.Forms.Button btnGoHome;
    }
}