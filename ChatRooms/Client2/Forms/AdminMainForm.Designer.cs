﻿namespace ChatRooms
{
    partial class AdminMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminMainForm));
            this.btnDnlPhoto = new System.Windows.Forms.Button();
            this.pnlPhoto = new System.Windows.Forms.Panel();
            this.lblAdminSign = new System.Windows.Forms.Label();
            this.pnlUsers = new System.Windows.Forms.Panel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.pnlUserList = new System.Windows.Forms.Panel();
            this.lvUsers = new System.Windows.Forms.ListView();
            this.pnlBanDBtn = new System.Windows.Forms.Panel();
            this.lblUserList = new System.Windows.Forms.Label();
            this.pnlRooms = new System.Windows.Forms.Panel();
            this.pnlAddRoomBtn = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lvRooms = new System.Windows.Forms.ListView();
            this.lblRooms = new System.Windows.Forms.Label();
            this.pnlRoomMsg = new System.Windows.Forms.Panel();
            this.pnlDelRoomDBtn = new System.Windows.Forms.Panel();
            this.pnlSendMsgBtn = new System.Windows.Forms.Panel();
            this.pnlLeaveRoomBtn = new System.Windows.Forms.Panel();
            this.pnlMsg = new System.Windows.Forms.Panel();
            this.rtbMsg = new System.Windows.Forms.RichTextBox();
            this.lblRoomName = new System.Windows.Forms.Label();
            this.txtSendMsg = new System.Windows.Forms.TextBox();
            this.pnlMyDialogs = new System.Windows.Forms.Panel();
            this.btnRequests = new System.Windows.Forms.Button();
            this.pnlRequests = new System.Windows.Forms.Panel();
            this.lvRoomToDel = new System.Windows.Forms.ListView();
            this.pnlDelRoomBtn = new System.Windows.Forms.Panel();
            this.pnlUnknownBtn = new System.Windows.Forms.Panel();
            this.pnlUserToBan = new System.Windows.Forms.Panel();
            this.lvUserToBan = new System.Windows.Forms.ListView();
            this.pnlBanBtn = new System.Windows.Forms.Panel();
            this.cbBanOptions = new System.Windows.Forms.ComboBox();
            this.pnlReqTitle = new System.Windows.Forms.Panel();
            this.lblReqTitle = new System.Windows.Forms.Label();
            this.btnMyDialogs = new System.Windows.Forms.Button();
            this.btnMyPage = new System.Windows.Forms.Button();
            this.pnlMyPage = new System.Windows.Forms.Panel();
            this.pnlExitBtn = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlCreateRoom = new System.Windows.Forms.Panel();
            this.pnlCreateRoomBtn = new System.Windows.Forms.Panel();
            this.txtNewRoomName = new System.Windows.Forms.TextBox();
            this.pnlPhoto.SuspendLayout();
            this.pnlUsers.SuspendLayout();
            this.pnlUserList.SuspendLayout();
            this.pnlRooms.SuspendLayout();
            this.pnlRoomMsg.SuspendLayout();
            this.pnlMsg.SuspendLayout();
            this.pnlMyDialogs.SuspendLayout();
            this.pnlRequests.SuspendLayout();
            this.pnlUserToBan.SuspendLayout();
            this.pnlReqTitle.SuspendLayout();
            this.pnlMyPage.SuspendLayout();
            this.pnlCreateRoom.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDnlPhoto
            // 
            this.btnDnlPhoto.Location = new System.Drawing.Point(42, 217);
            this.btnDnlPhoto.Name = "btnDnlPhoto";
            this.btnDnlPhoto.Size = new System.Drawing.Size(131, 23);
            this.btnDnlPhoto.TabIndex = 1;
            this.btnDnlPhoto.Text = "Загрузить фото";
            this.btnDnlPhoto.UseVisualStyleBackColor = true;
            // 
            // pnlPhoto
            // 
            this.pnlPhoto.BackColor = System.Drawing.SystemColors.Control;
            this.pnlPhoto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlPhoto.BackgroundImage")));
            this.pnlPhoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPhoto.Controls.Add(this.lblAdminSign);
            this.pnlPhoto.Location = new System.Drawing.Point(18, 13);
            this.pnlPhoto.Name = "pnlPhoto";
            this.pnlPhoto.Size = new System.Drawing.Size(177, 198);
            this.pnlPhoto.TabIndex = 0;
            // 
            // lblAdminSign
            // 
            this.lblAdminSign.AutoSize = true;
            this.lblAdminSign.Font = new System.Drawing.Font("Britannic Bold", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdminSign.Location = new System.Drawing.Point(140, -4);
            this.lblAdminSign.Name = "lblAdminSign";
            this.lblAdminSign.Size = new System.Drawing.Size(32, 42);
            this.lblAdminSign.TabIndex = 0;
            this.lblAdminSign.Text = "!";
            // 
            // pnlUsers
            // 
            this.pnlUsers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlUsers.Controls.Add(this.comboBox1);
            this.pnlUsers.Controls.Add(this.pnlUserList);
            this.pnlUsers.Controls.Add(this.pnlBanDBtn);
            this.pnlUsers.Controls.Add(this.lblUserList);
            this.pnlUsers.Location = new System.Drawing.Point(12, 257);
            this.pnlUsers.Name = "pnlUsers";
            this.pnlUsers.Size = new System.Drawing.Size(510, 110);
            this.pnlUsers.TabIndex = 5;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Period of Ban",
            "5 мин",
            "15 мин",
            "30 мин",
            "1 час",
            "1 сутки",
            "перманентный бан"});
            this.comboBox1.Location = new System.Drawing.Point(358, 6);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(140, 21);
            this.comboBox1.TabIndex = 3;
            // 
            // pnlUserList
            // 
            this.pnlUserList.Controls.Add(this.lvUsers);
            this.pnlUserList.Location = new System.Drawing.Point(4, 26);
            this.pnlUserList.Name = "pnlUserList";
            this.pnlUserList.Size = new System.Drawing.Size(332, 81);
            this.pnlUserList.TabIndex = 2;
            // 
            // lvUsers
            // 
            this.lvUsers.Location = new System.Drawing.Point(-1, -1);
            this.lvUsers.Name = "lvUsers";
            this.lvUsers.Size = new System.Drawing.Size(276, 79);
            this.lvUsers.TabIndex = 5;
            this.lvUsers.UseCompatibleStateImageBehavior = false;
            // 
            // pnlBanDBtn
            // 
            this.pnlBanDBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlBanDBtn.BackgroundImage")));
            this.pnlBanDBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlBanDBtn.Location = new System.Drawing.Point(322, -3);
            this.pnlBanDBtn.Name = "pnlBanDBtn";
            this.pnlBanDBtn.Size = new System.Drawing.Size(33, 33);
            this.pnlBanDBtn.TabIndex = 1;
            // 
            // lblUserList
            // 
            this.lblUserList.AutoSize = true;
            this.lblUserList.Location = new System.Drawing.Point(15, 9);
            this.lblUserList.Name = "lblUserList";
            this.lblUserList.Size = new System.Drawing.Size(124, 13);
            this.lblUserList.TabIndex = 0;
            this.lblUserList.Text = "Список пользователей";
            // 
            // pnlRooms
            // 
            this.pnlRooms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRooms.Controls.Add(this.pnlAddRoomBtn);
            this.pnlRooms.Controls.Add(this.textBox1);
            this.pnlRooms.Controls.Add(this.lvRooms);
            this.pnlRooms.Controls.Add(this.lblRooms);
            this.pnlRooms.Location = new System.Drawing.Point(366, 13);
            this.pnlRooms.Name = "pnlRooms";
            this.pnlRooms.Size = new System.Drawing.Size(156, 238);
            this.pnlRooms.TabIndex = 4;
            // 
            // pnlAddRoomBtn
            // 
            this.pnlAddRoomBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlAddRoomBtn.BackgroundImage")));
            this.pnlAddRoomBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlAddRoomBtn.Location = new System.Drawing.Point(117, -2);
            this.pnlAddRoomBtn.Name = "pnlAddRoomBtn";
            this.pnlAddRoomBtn.Size = new System.Drawing.Size(33, 33);
            this.pnlAddRoomBtn.TabIndex = 3;
            this.pnlAddRoomBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlAddRoomBtn_MouseClick);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(33, 212);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(88, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "Поиск";
            // 
            // lvRooms
            // 
            this.lvRooms.Location = new System.Drawing.Point(6, 33);
            this.lvRooms.Name = "lvRooms";
            this.lvRooms.Size = new System.Drawing.Size(138, 176);
            this.lvRooms.TabIndex = 4;
            this.lvRooms.UseCompatibleStateImageBehavior = false;
            // 
            // lblRooms
            // 
            this.lblRooms.AutoSize = true;
            this.lblRooms.Location = new System.Drawing.Point(12, 9);
            this.lblRooms.Name = "lblRooms";
            this.lblRooms.Size = new System.Drawing.Size(53, 13);
            this.lblRooms.TabIndex = 0;
            this.lblRooms.Text = "Комнаты";
            // 
            // pnlRoomMsg
            // 
            this.pnlRoomMsg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRoomMsg.Controls.Add(this.pnlDelRoomDBtn);
            this.pnlRoomMsg.Controls.Add(this.pnlSendMsgBtn);
            this.pnlRoomMsg.Controls.Add(this.pnlLeaveRoomBtn);
            this.pnlRoomMsg.Controls.Add(this.pnlMsg);
            this.pnlRoomMsg.Controls.Add(this.lblRoomName);
            this.pnlRoomMsg.Controls.Add(this.txtSendMsg);
            this.pnlRoomMsg.Location = new System.Drawing.Point(12, 12);
            this.pnlRoomMsg.Name = "pnlRoomMsg";
            this.pnlRoomMsg.Size = new System.Drawing.Size(347, 239);
            this.pnlRoomMsg.TabIndex = 3;
            // 
            // pnlDelRoomDBtn
            // 
            this.pnlDelRoomDBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlDelRoomDBtn.BackgroundImage")));
            this.pnlDelRoomDBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlDelRoomDBtn.Location = new System.Drawing.Point(314, 3);
            this.pnlDelRoomDBtn.Name = "pnlDelRoomDBtn";
            this.pnlDelRoomDBtn.Size = new System.Drawing.Size(30, 30);
            this.pnlDelRoomDBtn.TabIndex = 4;
            // 
            // pnlSendMsgBtn
            // 
            this.pnlSendMsgBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlSendMsgBtn.BackgroundImage")));
            this.pnlSendMsgBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlSendMsgBtn.Location = new System.Drawing.Point(296, 202);
            this.pnlSendMsgBtn.Name = "pnlSendMsgBtn";
            this.pnlSendMsgBtn.Size = new System.Drawing.Size(33, 33);
            this.pnlSendMsgBtn.TabIndex = 0;
            // 
            // pnlLeaveRoomBtn
            // 
            this.pnlLeaveRoomBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlLeaveRoomBtn.BackgroundImage")));
            this.pnlLeaveRoomBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlLeaveRoomBtn.Location = new System.Drawing.Point(285, 3);
            this.pnlLeaveRoomBtn.Name = "pnlLeaveRoomBtn";
            this.pnlLeaveRoomBtn.Size = new System.Drawing.Size(30, 30);
            this.pnlLeaveRoomBtn.TabIndex = 3;
            // 
            // pnlMsg
            // 
            this.pnlMsg.Controls.Add(this.rtbMsg);
            this.pnlMsg.Location = new System.Drawing.Point(3, 26);
            this.pnlMsg.Name = "pnlMsg";
            this.pnlMsg.Size = new System.Drawing.Size(297, 184);
            this.pnlMsg.TabIndex = 2;
            // 
            // rtbMsg
            // 
            this.rtbMsg.Location = new System.Drawing.Point(-4, 0);
            this.rtbMsg.Name = "rtbMsg";
            this.rtbMsg.Size = new System.Drawing.Size(280, 184);
            this.rtbMsg.TabIndex = 4;
            this.rtbMsg.Text = "";
            // 
            // lblRoomName
            // 
            this.lblRoomName.AutoSize = true;
            this.lblRoomName.Location = new System.Drawing.Point(15, 10);
            this.lblRoomName.Name = "lblRoomName";
            this.lblRoomName.Size = new System.Drawing.Size(105, 13);
            this.lblRoomName.TabIndex = 1;
            this.lblRoomName.Text = "Название комнаты";
            // 
            // txtSendMsg
            // 
            this.txtSendMsg.Location = new System.Drawing.Point(3, 213);
            this.txtSendMsg.Name = "txtSendMsg";
            this.txtSendMsg.Size = new System.Drawing.Size(276, 20);
            this.txtSendMsg.TabIndex = 0;
            this.txtSendMsg.Text = "Отправить";
            // 
            // pnlMyDialogs
            // 
            this.pnlMyDialogs.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlMyDialogs.BackColor = System.Drawing.Color.Transparent;
            this.pnlMyDialogs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMyDialogs.Controls.Add(this.pnlUsers);
            this.pnlMyDialogs.Controls.Add(this.pnlRoomMsg);
            this.pnlMyDialogs.Controls.Add(this.pnlRooms);
            this.pnlMyDialogs.Location = new System.Drawing.Point(184, 38);
            this.pnlMyDialogs.Name = "pnlMyDialogs";
            this.pnlMyDialogs.Size = new System.Drawing.Size(548, 407);
            this.pnlMyDialogs.TabIndex = 14;
            this.pnlMyDialogs.Visible = false;
            // 
            // btnRequests
            // 
            this.btnRequests.Location = new System.Drawing.Point(85, 113);
            this.btnRequests.Name = "btnRequests";
            this.btnRequests.Size = new System.Drawing.Size(99, 23);
            this.btnRequests.TabIndex = 13;
            this.btnRequests.Text = "Заявки";
            this.btnRequests.UseVisualStyleBackColor = true;
            this.btnRequests.Click += new System.EventHandler(this.btnRequests_Click);
            // 
            // pnlRequests
            // 
            this.pnlRequests.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlRequests.BackColor = System.Drawing.Color.Transparent;
            this.pnlRequests.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRequests.Controls.Add(this.lvRoomToDel);
            this.pnlRequests.Controls.Add(this.pnlDelRoomBtn);
            this.pnlRequests.Controls.Add(this.pnlUnknownBtn);
            this.pnlRequests.Controls.Add(this.pnlUserToBan);
            this.pnlRequests.Controls.Add(this.pnlReqTitle);
            this.pnlRequests.Location = new System.Drawing.Point(184, 38);
            this.pnlRequests.Name = "pnlRequests";
            this.pnlRequests.Size = new System.Drawing.Size(548, 407);
            this.pnlRequests.TabIndex = 15;
            this.pnlRequests.Visible = false;
            // 
            // lvRoomToDel
            // 
            this.lvRoomToDel.Location = new System.Drawing.Point(27, 71);
            this.lvRoomToDel.Name = "lvRoomToDel";
            this.lvRoomToDel.Size = new System.Drawing.Size(173, 258);
            this.lvRoomToDel.TabIndex = 8;
            this.lvRoomToDel.UseCompatibleStateImageBehavior = false;
            // 
            // pnlDelRoomBtn
            // 
            this.pnlDelRoomBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlDelRoomBtn.BackgroundImage")));
            this.pnlDelRoomBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlDelRoomBtn.Location = new System.Drawing.Point(196, 74);
            this.pnlDelRoomBtn.Name = "pnlDelRoomBtn";
            this.pnlDelRoomBtn.Size = new System.Drawing.Size(28, 28);
            this.pnlDelRoomBtn.TabIndex = 2;
            // 
            // pnlUnknownBtn
            // 
            this.pnlUnknownBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlUnknownBtn.BackgroundImage")));
            this.pnlUnknownBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlUnknownBtn.Location = new System.Drawing.Point(66, 18);
            this.pnlUnknownBtn.Name = "pnlUnknownBtn";
            this.pnlUnknownBtn.Size = new System.Drawing.Size(33, 35);
            this.pnlUnknownBtn.TabIndex = 6;
            // 
            // pnlUserToBan
            // 
            this.pnlUserToBan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlUserToBan.Controls.Add(this.lvUserToBan);
            this.pnlUserToBan.Controls.Add(this.pnlBanBtn);
            this.pnlUserToBan.Controls.Add(this.cbBanOptions);
            this.pnlUserToBan.Location = new System.Drawing.Point(230, 71);
            this.pnlUserToBan.Name = "pnlUserToBan";
            this.pnlUserToBan.Size = new System.Drawing.Size(290, 258);
            this.pnlUserToBan.TabIndex = 7;
            // 
            // lvUserToBan
            // 
            this.lvUserToBan.Location = new System.Drawing.Point(17, 3);
            this.lvUserToBan.Name = "lvUserToBan";
            this.lvUserToBan.Size = new System.Drawing.Size(194, 217);
            this.lvUserToBan.TabIndex = 9;
            this.lvUserToBan.UseCompatibleStateImageBehavior = false;
            // 
            // pnlBanBtn
            // 
            this.pnlBanBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlBanBtn.BackgroundImage")));
            this.pnlBanBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlBanBtn.Location = new System.Drawing.Point(229, 215);
            this.pnlBanBtn.Name = "pnlBanBtn";
            this.pnlBanBtn.Size = new System.Drawing.Size(29, 29);
            this.pnlBanBtn.TabIndex = 1;
            // 
            // cbBanOptions
            // 
            this.cbBanOptions.FormattingEnabled = true;
            this.cbBanOptions.Items.AddRange(new object[] {
            "Period of Ban",
            "5 мин",
            "15 мин",
            "30 мин",
            "1 час",
            "1 сутки",
            "перманентный бан"});
            this.cbBanOptions.Location = new System.Drawing.Point(17, 223);
            this.cbBanOptions.Name = "cbBanOptions";
            this.cbBanOptions.Size = new System.Drawing.Size(140, 21);
            this.cbBanOptions.TabIndex = 0;
            // 
            // pnlReqTitle
            // 
            this.pnlReqTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlReqTitle.Controls.Add(this.lblReqTitle);
            this.pnlReqTitle.Location = new System.Drawing.Point(105, 8);
            this.pnlReqTitle.Name = "pnlReqTitle";
            this.pnlReqTitle.Size = new System.Drawing.Size(363, 49);
            this.pnlReqTitle.TabIndex = 4;
            // 
            // lblReqTitle
            // 
            this.lblReqTitle.AutoSize = true;
            this.lblReqTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblReqTitle.Location = new System.Drawing.Point(22, 4);
            this.lblReqTitle.Name = "lblReqTitle";
            this.lblReqTitle.Size = new System.Drawing.Size(314, 40);
            this.lblReqTitle.TabIndex = 0;
            this.lblReqTitle.Text = "Запросы на удаление диалогов или бан\r\nпользователей";
            this.lblReqTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnMyDialogs
            // 
            this.btnMyDialogs.Location = new System.Drawing.Point(85, 91);
            this.btnMyDialogs.Name = "btnMyDialogs";
            this.btnMyDialogs.Size = new System.Drawing.Size(99, 23);
            this.btnMyDialogs.TabIndex = 12;
            this.btnMyDialogs.Text = "Мои диалоги";
            this.btnMyDialogs.UseVisualStyleBackColor = true;
            this.btnMyDialogs.Click += new System.EventHandler(this.btnMyDialogs_Click_1);
            // 
            // btnMyPage
            // 
            this.btnMyPage.Location = new System.Drawing.Point(85, 69);
            this.btnMyPage.Name = "btnMyPage";
            this.btnMyPage.Size = new System.Drawing.Size(99, 23);
            this.btnMyPage.TabIndex = 11;
            this.btnMyPage.Text = "Моя страница";
            this.btnMyPage.UseVisualStyleBackColor = true;
            this.btnMyPage.Click += new System.EventHandler(this.btnMyPage_Click);
            // 
            // pnlMyPage
            // 
            this.pnlMyPage.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlMyPage.BackColor = System.Drawing.Color.Transparent;
            this.pnlMyPage.Controls.Add(this.btnDnlPhoto);
            this.pnlMyPage.Controls.Add(this.pnlPhoto);
            this.pnlMyPage.Location = new System.Drawing.Point(194, 19);
            this.pnlMyPage.Name = "pnlMyPage";
            this.pnlMyPage.Size = new System.Drawing.Size(521, 257);
            this.pnlMyPage.TabIndex = 10;
            // 
            // pnlExitBtn
            // 
            this.pnlExitBtn.BackColor = System.Drawing.Color.Transparent;
            this.pnlExitBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlExitBtn.BackgroundImage")));
            this.pnlExitBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlExitBtn.Location = new System.Drawing.Point(152, 41);
            this.pnlExitBtn.Name = "pnlExitBtn";
            this.pnlExitBtn.Size = new System.Drawing.Size(23, 23);
            this.pnlExitBtn.TabIndex = 9;
            this.pnlExitBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlExitBtn_MouseClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(89, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "tempLogin";
            // 
            // pnlCreateRoom
            // 
            this.pnlCreateRoom.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlCreateRoom.BackColor = System.Drawing.Color.Transparent;
            this.pnlCreateRoom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCreateRoom.Controls.Add(this.pnlCreateRoomBtn);
            this.pnlCreateRoom.Controls.Add(this.txtNewRoomName);
            this.pnlCreateRoom.Location = new System.Drawing.Point(184, 38);
            this.pnlCreateRoom.Name = "pnlCreateRoom";
            this.pnlCreateRoom.Size = new System.Drawing.Size(548, 407);
            this.pnlCreateRoom.TabIndex = 16;
            this.pnlCreateRoom.Visible = false;
            // 
            // pnlCreateRoomBtn
            // 
            this.pnlCreateRoomBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlCreateRoomBtn.BackgroundImage")));
            this.pnlCreateRoomBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlCreateRoomBtn.Location = new System.Drawing.Point(483, 19);
            this.pnlCreateRoomBtn.Name = "pnlCreateRoomBtn";
            this.pnlCreateRoomBtn.Size = new System.Drawing.Size(33, 33);
            this.pnlCreateRoomBtn.TabIndex = 4;
            this.pnlCreateRoomBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlCreateRoomBtn_MouseClick);
            // 
            // txtNewRoomName
            // 
            this.txtNewRoomName.Location = new System.Drawing.Point(42, 26);
            this.txtNewRoomName.Name = "txtNewRoomName";
            this.txtNewRoomName.Size = new System.Drawing.Size(415, 20);
            this.txtNewRoomName.TabIndex = 0;
            this.txtNewRoomName.Text = "Введите название беседы";
            // 
            // AdminMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(817, 465);
            this.Controls.Add(this.pnlCreateRoom);
            this.Controls.Add(this.pnlMyDialogs);
            this.Controls.Add(this.btnRequests);
            this.Controls.Add(this.pnlRequests);
            this.Controls.Add(this.btnMyDialogs);
            this.Controls.Add(this.btnMyPage);
            this.Controls.Add(this.pnlExitBtn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pnlMyPage);
            this.Name = "AdminMainForm";
            this.Text = "Chat";
            this.Load += new System.EventHandler(this.AdminMainForm_Load);
            this.pnlPhoto.ResumeLayout(false);
            this.pnlPhoto.PerformLayout();
            this.pnlUsers.ResumeLayout(false);
            this.pnlUsers.PerformLayout();
            this.pnlUserList.ResumeLayout(false);
            this.pnlRooms.ResumeLayout(false);
            this.pnlRooms.PerformLayout();
            this.pnlRoomMsg.ResumeLayout(false);
            this.pnlRoomMsg.PerformLayout();
            this.pnlMsg.ResumeLayout(false);
            this.pnlMyDialogs.ResumeLayout(false);
            this.pnlRequests.ResumeLayout(false);
            this.pnlUserToBan.ResumeLayout(false);
            this.pnlReqTitle.ResumeLayout(false);
            this.pnlReqTitle.PerformLayout();
            this.pnlMyPage.ResumeLayout(false);
            this.pnlCreateRoom.ResumeLayout(false);
            this.pnlCreateRoom.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnDnlPhoto;
        private System.Windows.Forms.Panel pnlPhoto;
        private System.Windows.Forms.Label lblAdminSign;
        private System.Windows.Forms.Panel pnlUsers;
        private System.Windows.Forms.Panel pnlUserList;
        private System.Windows.Forms.Panel pnlBanDBtn;
        private System.Windows.Forms.Label lblUserList;
        private System.Windows.Forms.Panel pnlRooms;
        private System.Windows.Forms.Panel pnlAddRoomBtn;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblRooms;
        private System.Windows.Forms.Panel pnlRoomMsg;
        private System.Windows.Forms.Panel pnlSendMsgBtn;
        private System.Windows.Forms.Panel pnlLeaveRoomBtn;
        private System.Windows.Forms.Panel pnlMsg;
        private System.Windows.Forms.Label lblRoomName;
        private System.Windows.Forms.TextBox txtSendMsg;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Panel pnlDelRoomDBtn;
        private System.Windows.Forms.ListView lvUsers;
        private System.Windows.Forms.ListView lvRooms;
        private System.Windows.Forms.RichTextBox rtbMsg;
        private System.Windows.Forms.Panel pnlMyDialogs;
        private System.Windows.Forms.Button btnRequests;
        private System.Windows.Forms.Panel pnlRequests;
        private System.Windows.Forms.ListView lvRoomToDel;
        private System.Windows.Forms.Panel pnlDelRoomBtn;
        private System.Windows.Forms.Panel pnlUnknownBtn;
        private System.Windows.Forms.Panel pnlUserToBan;
        private System.Windows.Forms.ListView lvUserToBan;
        private System.Windows.Forms.Panel pnlBanBtn;
        private System.Windows.Forms.ComboBox cbBanOptions;
        private System.Windows.Forms.Panel pnlReqTitle;
        private System.Windows.Forms.Label lblReqTitle;
        private System.Windows.Forms.Button btnMyDialogs;
        private System.Windows.Forms.Button btnMyPage;
        private System.Windows.Forms.Panel pnlMyPage;
        private System.Windows.Forms.Panel pnlExitBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pnlCreateRoom;
        private System.Windows.Forms.Panel pnlCreateRoomBtn;
        private System.Windows.Forms.TextBox txtNewRoomName;
    }
}