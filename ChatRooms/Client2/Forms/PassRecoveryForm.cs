﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChatModels.Exceptions;

namespace ChatRooms
{
    public partial class PassRecoveryForm : Form
    {

        Form parrent;
        public PassRecoveryForm(Form parrent)
        {
            this.parrent = parrent;
            InitializeComponent();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            String emailStr = txtEMail.Text;

            try
            {
                ConfirmEmail(emailStr);
                MessageBox.Show("Message sended");
                parrent.Show();
                this.Close();
            }
            catch (EmailNotFoundException ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private bool ConfirmEmail(String email)
        {
            return new AREModule().Forget(email);
        }
    }
}
