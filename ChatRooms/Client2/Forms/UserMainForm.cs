﻿using ChatModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatRooms
{
    public partial class UserMainForm : Form
    {
        ChatEngine engine = new ChatEngine();
        User user;
        Form form;
        public UserMainForm(Form form, User user)
        {
            this.form = form;
            this.user = user;
            InitializeComponent();
            lblFnameInfo.Text = user.FirstName;
            lblLNameInfo.Text = user.LastName;
            lblMailInfo.Text = user.Email;
            lblPhoneInfo.Text = user.Phone;
            lblUserLogin.Text = user.Login;
        }
        private void btnMyPage_Click(object sender, EventArgs e)
        {
            pnlMyPage.Visible = true;
            pnlMyDialogs.Visible = false;
            pnlSettings.Visible = false;
            pnlCreateRoom.Visible = false;
            pnlExitBtn.Visible = true;
            pnlExitBtn.Enabled = true;
        }

        private void btnMyDialogs_Click(object sender, EventArgs e)
        {
            pnlMyPage.Visible = false;
            pnlSettings.Visible = false;
            pnlMyDialogs.Visible = true;
            pnlCreateRoom.Visible = false;
            pnlExitBtn.Visible = false;
            pnlExitBtn.Enabled = false;
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            pnlSettings.Visible = true;
            pnlMyDialogs.Visible = false;
            pnlMyPage.Visible = false;
            pnlExitBtn.Visible = false;
            pnlCreateRoom.Visible = false;
            pnlExitBtn.Enabled = false;
        }

        private void UserMainForm_Load(object sender, EventArgs e)
        {
            pnlMyPage.Visible = true;
            pnlMyDialogs.Visible = false;
            pnlSettings.Visible = false;
            pnlCreateRoom.Visible = false;
        }
        private void pnlAddRoomBtn_MouseClick(object sender, MouseEventArgs e)
        {
            pnlMyDialogs.Visible = false;
            pnlCreateRoom.Visible = true;
        }
        private void pnlCreateRoomBtn_MouseClick(object sender, MouseEventArgs e)
        {
            pnlCreateRoom.Visible = false;
            pnlMyDialogs.Visible = true;
        }
        private void pnlExitBtn_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                bool logOutResponse = engine.LogOut(user);
                if(logOutResponse == true)
                {
                    form.Show();
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
