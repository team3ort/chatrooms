﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;
using ChatModels.Exceptions;
using Newtonsoft.Json;

namespace ServerChatRooms
{
    public class CommandParser
    {
        AREModule areModule = new AREModule();
        RoomsMobule roomsModule = new RoomsMobule();
        MessageModule messageModule = new MessageModule();

        public void Response(string input, int currentClient)                           
        {
            Dictionary<string, string> data = JsonConvert.DeserializeObject<Dictionary<string, string>>(input);

            switch (data["cmd"])
            {

                case "login":
                    User loginUser = new User(data["login"], data["password"]);
                    areModule.Login(loginUser, currentClient);
                    break;

                case "register":
                    User regUser = new User(data["login"], data["password"],
                                              data["fname"], data["lname"],
                                              data["email"], data["phone"]);
                    areModule.Register(regUser, currentClient);
                    break;

                case "forget":
                    string email = data["email"];
                    areModule.Forget(email, currentClient);
                    break;

                case "logout":
                    User logoutUser = new User(data["login"], data["password"]);
                    areModule.LogOut(logoutUser, currentClient);
                    break;

                case "getallrooms":
                    roomsModule.getAllRooms(currentClient);
                    break;

                case "userenterinroom":
                    Room roomEnter = roomsModule.FindRoom(data["room"]);
                    User userEnter = AREModule.FindUser(ServerData.ConnectedClients[currentClient].User.Login);
                    roomsModule.UserEnterInRoom(userEnter, roomEnter);
                    break;

                case "message":
                    Room room1 = roomsModule.FindRoom(data["roomname"]);
                    User author = AREModule.FindUser(data["author"]);
                    Message message = new Message(author, room1, data["msgdata"]);
                    messageModule.getMessage(message);
                    break;

                case "leaveroom":
                    User outUser = AREModule.FindUser(ServerData.ConnectedClients[currentClient].User.Login);
                    Room outFromRoom = roomsModule.FindRoom(data["room"]);
                    roomsModule.OutFromRoom(outUser, outFromRoom);
                    break;

                case "createroom":
                    User userCreater = AREModule.FindUser(ServerData.ConnectedClients[currentClient].User.Login);
                    Room newRoom = new Room(data["room"]);
                    roomsModule.CreateRoom(newRoom, userCreater);
                    break;

                case "userdisconnect":
                    User userDisconnect = AREModule.FindUser(data["user"]);
                    roomsModule.DisconnectUser(userDisconnect);
                    break;

                default:
                    throw new InvalidOperationException("Неверная команда серверу");
                    break;
            }
}
    }
}
