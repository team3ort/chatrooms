﻿using ChatModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ServerChatRooms
{
    public static class ServerData
    {
        private static List<Client> connectedClients;

        public static List<Client> ConnectedClients
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return connectedClients;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                connectedClients = value;
            }
        }

        public static void Init()
        {
            ConnectedClients = new List<Client>();
        }

        public static int findUserIDInConnection(String login)
        {
            int result = -1;
            for (int i = 0; i < connectedClients.Count; i++)
            {
                if (connectedClients[i].User == null)
                    continue;

                if (login == connectedClients[i].User.Login)
                {
                    result = i;
                    return result;
                }
            }
            return result;
        }

    }
}
