﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerChatRooms
{
    class Program
    {
        static void Main(string[] args)
        {
            ServerTCP server = new ServerTCP();
            ServerData.Init();

            /*//*************************************** 
            Dictionary<string, string> request = new Dictionary<string, string>();
            request.Add("cmd", "getallrooms");
            string requestStr = JsonConvert.SerializeObject(request);

            CommandParser cp = new CommandParser();
            cp.Response(requestStr, 0);
            //**************************************   */

            /*//*************************************** 
            Dictionary<string, string> request = new Dictionary<string, string>();
            request.Add("cmd", "getallmessageinroom");
            ChatModels.Room room = new ChatModels.Room("room0");
            request.Add("room", room.Name);
            string requestStr = JsonConvert.SerializeObject(request);

            CommandParser cp = new CommandParser();
            cp.Response(requestStr, 0);
            //**************************************   */

            server.StartServer();
        }
    }
}
