﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerChatRooms
{
    interface IDAO<T>
    {
        void Create(T t);
        List<T> Read();
        void Update(T t);
        T Delete(T t);
    }
}
