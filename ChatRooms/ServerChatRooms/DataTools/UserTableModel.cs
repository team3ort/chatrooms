﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;

namespace ServerChatRooms
{
    public class UserTableModel
    {
        IDAO<User> table = new DAOMockDB<User>(MockDB.Users);

        public UserTableModel()
        {

        }       

        public void Create (User user)
        {
            table.Create(user);
        }

        public List<User> Read()
        {
            return table.Read();
        }

        public void Update(User user)
        {
            table.Update(user);
        }
        
        public User Delete(User user)
        {
            return table.Delete(user);
        }
    }
}
