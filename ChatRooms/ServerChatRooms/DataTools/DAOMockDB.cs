﻿using ChatModels;
using ChatModels.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerChatRooms
{
    public class DAOMockDB<T> : IDAO<T>
    {
        List<T> data;
        public DAOMockDB(List<T> data)
        {
            this.data = data;
        }

        public void Create(T t)
        {
            if (!data.Contains(t))
            {
                data.Add(t);
            }
            else
                throw new ArgumentException("Field already exist");
        }

        public T Delete(T t)
        {
            T result;
            for(int i = 0; i < data.Count; i++)
            {
                if(data[i].Equals(t))
                {
                    result = data[i];
                    data.RemoveAt(i);
                    return result;
                }               
            }
            throw new ArgumentException("Field not found");
        }

        public List<T> Read()
        {
            return data;
        }

        public void Update(T t)
        {
            bool isFound = false;
            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].Equals(t))
                {
                    data[i] = t;
                    isFound = true;
                }
            }
            if (!isFound)
                throw new ArgumentException("Field not found");
        }
    }
}
