﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;

namespace ServerChatRooms
{
    public class RoomTableModel
    {
        IDAO<Room> table = new DAOMockDB<Room>(MockDB.Rooms);

        public RoomTableModel()
        {

        }
        public RoomTableModel(List<Room> data)
        {
            IDAO<Room> table = new DAOMockDB<Room>(data);
        }

        public void Create(Room room)
        {
            table.Create(room);
        }

        public List<Room> Read()
        {
            return table.Read();
        }

        public void Update(Room room)
        {
            table.Update(room);
        }

        public Room Delete(Room room)
        {
            return table.Delete(room);
        }
    }
}
