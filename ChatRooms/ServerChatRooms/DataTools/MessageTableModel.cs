﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;

namespace ServerChatRooms
{
    class MessageTableModel
    {
        IDAO<Message> table = new DAOMockDB<Message>(MockDB.Messages);

        public MessageTableModel()
        {

        }
        public MessageTableModel(List<Message> data)
        {
            IDAO<Message> table = new DAOMockDB<Message>(data);
        }

        public void Create(Message message)
        {
            table.Create(message);
        }

        public List<Message> Read()
        {
            return table.Read();
        }

        public void Update(Message message)
        {
            table.Update(message);
        }

        public Message Delete(Message message)
        {
            return table.Delete(message);
        }
    }
}
