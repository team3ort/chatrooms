﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;
using Newtonsoft.Json;

namespace ServerChatRooms
{
    public class MessageModule
    {
        public void getMessage(Message message)
        {            
            MockDB.Messages.Add(message);                                           // Положить в БД Message
            sendMessageForUsersInRoom(message.Room, message);
        }

        private void sendMessageForUsersInRoom(Room room, Message message)
        {
            List<User> usersInRoom = room.UsersInRoom;

            for (int i = 0; i < usersInRoom.Count; i++)
            {
                for (int j = 0; j < ServerData.ConnectedClients.Count; j++)
                {
                    if (ServerData.ConnectedClients[j].User == null)
                        continue;

                    if (usersInRoom[i].Login == ServerData.ConnectedClients[j].User.Login)
                    {
                        ResponsePackage result = new ResponsePackage("message", JsonConvert.SerializeObject(message));
                        ServerTCP.SendResponse(ServerData.ConnectedClients[j], JsonConvert.SerializeObject(result));
                        break;
                    }
                }
            }
        }

        public static List<Message> getMessagesInRoom(Room room)
        {
            List<Message> resultList = new List<Message>();

            for (int i = 0; i < MockDB.Messages.Count; i++)
            {
                if (room.Name == MockDB.Messages[i].Room.Name)
                {
                    resultList.Add(MockDB.Messages[i]);
                }
            }
            return resultList;
        }
    }
}
