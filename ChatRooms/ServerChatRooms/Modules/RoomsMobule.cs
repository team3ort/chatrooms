﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;
using Newtonsoft.Json;

namespace ServerChatRooms
{
    public class RoomsMobule
    {
        RoomTableModel roomsModel = new RoomTableModel();
        MessageTableModel messageInRoom = new MessageTableModel();

        // ******************************************   НЕЛЬЗЯ ДВЕ КОМНАТЫ С ОДИНАКОВЫМ ИМЕНЕМ!!!!

        public void getAllRooms(int currentUser)
        {
            ResponsePackage result = new ResponsePackage();
            List<Room> allRooms = roomsModel.Read();
            result = new ResponsePackage("rooms", JsonConvert.SerializeObject(allRooms));
            ServerTCP.SendResponse(ServerData.ConnectedClients[currentUser], JsonConvert.SerializeObject(result));
        }

        private void getAllMessageFromRoom(User user, Room room)
        {
            int currentUser = ServerData.findUserIDInConnection(user.Login);

            if (currentUser == -1)
            {
                //throw new ArgumentException("User not found");
            }
            else
            {
                ResponsePackage result = new ResponsePackage();
                //List<Message> allMessageInRoom = messageInRoom.Read();
                List<Message> allMessageInRoom = MessageModule.getMessagesInRoom(room);
                result = new ResponsePackage("messageinroom", JsonConvert.SerializeObject(allMessageInRoom));
                ServerTCP.SendResponse(ServerData.ConnectedClients[currentUser], JsonConvert.SerializeObject(result));
            }
        }

        public void UserEnterInRoom(User user, Room room)                   // Изменить список уже находящихся
        {
            room.UsersInRoom.Add(user.getName());
            sendUpdateClientsInRoom(room);
            getAllMessageFromRoom(user, room);
        }

        private void getAllUsersInRoom(User user, Room room)
        {
            int currentUser = ServerData.findUserIDInConnection(user.Login);

            if (currentUser == -1)
            {
                //throw new ArgumentException("User not found");
            }
            else
            {
                ResponsePackage result = new ResponsePackage();
                List<User> allUsersInRoom = room.UsersInRoom;
                result = new ResponsePackage("usersinroom", JsonConvert.SerializeObject(allUsersInRoom));
                ServerTCP.SendResponse(ServerData.ConnectedClients[currentUser], JsonConvert.SerializeObject(result));
            }           
        }

        public Room FindRoom(String roomName)
        {
            Room room = null;

            for (int i = 0; i < MockDB.Rooms.Count; i++)
            {
                if (roomName == MockDB.Rooms[i].Name)
                {
                    room = MockDB.Rooms[i];                                     // переделать
                    break;
                }
            }

            return room;
        }

        private void sendUpdateClientsInRoom(Room room)
        {
            for (int i = 0; i < room.UsersInRoom.Count; i++)
            {
                getAllUsersInRoom(room.UsersInRoom[i], room);
            }            
        }

        public void OutFromRoom(User user, Room room)
        {
            bool isLeave = false;
            for (int i = 0; i < room.UsersInRoom.Count; i++)
            {
                if(user.Login == room.UsersInRoom[i].Login)
                {
                    ResponsePackage result = new ResponsePackage();
                    isLeave = true;
                    result = new ResponsePackage("userleaveroom", JsonConvert.SerializeObject(isLeave));
                    int currentUser = ServerData.findUserIDInConnection(user.Login);
                    ServerTCP.SendResponse(ServerData.ConnectedClients[currentUser], JsonConvert.SerializeObject(result));

                    room.UsersInRoom.RemoveAt(i);
                    break;
                }
            }
            sendUpdateClientsInRoom(room);
        }

        private void sendUpdateRooms()
        {
            List<Room> rooms = roomsModel.Read();
            for (int i = 0; i < ServerData.ConnectedClients.Count; i++)
            {
                if(ServerData.ConnectedClients[i].User == null)
                {
                    continue;
                }

                getAllRooms(i);
            }
        }

        public void CreateRoom(Room room, User user)
        {
            room.Creator = user;
            roomsModel.Create(room);
            sendUpdateRooms();
        }

        public void DisconnectUser(User user)
        {
            List<Room> rooms = roomsModel.Read();
            Room currentRoom = null;
            bool isFound = false;

            for (int i = 0; i < rooms.Count && !isFound; i++)
            {
                for (int j = 0; j < rooms[i].UsersInRoom.Count; j++)
                {
                    if (user.Login == rooms[i].UsersInRoom[j].Login)
                    {
                        currentRoom = rooms[i];
                        currentRoom.UsersInRoom.RemoveAt(j);
                        isFound = true;
                        break;
                    }
                }
            }

            if (currentRoom != null)
                sendUpdateClientsInRoom(currentRoom);
        }
    }
}