﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;
using ChatModels.Exceptions;
using Newtonsoft.Json;
using System.Net;
using System.Web;

namespace ServerChatRooms
{
    public class AREModule
    {
        UserTableModel users = new UserTableModel();
        public void Login(User user, int currentUser)
        {
            ResponsePackage result = new ResponsePackage();

            List<User> userData = users.Read();                                 // сделать выборку на уровне БД

            bool isFound = false;
            for (int i = 0; i < userData.Count; i++)
            {
                if (userData[i].Login == user.Login && userData[i].Password == user.Password)
                {
                    isFound = true;
                    ServerData.ConnectedClients[currentUser].User = userData[i];                                    // *** переделать
                    result = new ResponsePackage("user", JsonConvert.SerializeObject(userData[i]));
                    break;
                }
            }
            if (!isFound)
            {
                result = new ResponsePackage("error", JsonConvert.SerializeObject(new Error("Login or pasword wrong")));
                ServerTCP.SendResponse(ServerData.ConnectedClients[currentUser], JsonConvert.SerializeObject(result));
                ServerData.ConnectedClients.RemoveAt(currentUser);                                                  // *** переделать
            }
            else
            {
                ServerTCP.SendResponse(ServerData.ConnectedClients[currentUser], JsonConvert.SerializeObject(result));
            }

        }

        public void Register(User user, int currentUser)
        {
            ResponsePackage result = new ResponsePackage();

            // Не проверяю валидацию данных

            try
            {
                users.Create(user);
                result = new ResponsePackage("bool", JsonConvert.SerializeObject(true));
            }
            catch (ArgumentException e)
            {
                result = new ResponsePackage("error", JsonConvert.SerializeObject(new Error("User already exist")));
            }

            ServerTCP.SendResponse(ServerData.ConnectedClients[currentUser], JsonConvert.SerializeObject(result));
            /******************************************************************    ВЕРНИ, КОГДА ПОПРАВИШЬ НА КЛИЕНТЕ     */
            //ServerData.ConnectedClients[currentUser].ClientTcp.Close();
            //ServerData.ConnectedClients.RemoveAt(currentUser);
        }

        public void Forget(string email, int currentUser)
        {
            string uncEmail = HttpUtility.UrlDecode(email);
            bool isFound = false;
            ResponsePackage result = new ResponsePackage();

            List<User> userData = users.Read();

            for (int i = 0; i < userData.Count; i++)
            {
                if (userData[i].Email == uncEmail)
                {
                    isFound = true;
                    break;
                }
            }

            if (isFound)
                result = new ResponsePackage("bool", JsonConvert.SerializeObject(true));
            else
                result = new ResponsePackage("error", JsonConvert.SerializeObject(new Error("Email not found")));

            ServerTCP.SendResponse(ServerData.ConnectedClients[currentUser], JsonConvert.SerializeObject(result));
            ServerData.ConnectedClients[currentUser].ClientTcp.Close();
            ServerData.ConnectedClients.RemoveAt(currentUser);
        }

        public void LogOut(User user, int currentUser)
        {
            ResponsePackage result = new ResponsePackage();
            bool isFound = false;


            if (ServerData.ConnectedClients[currentUser].User.Equals(user))
            {
                isFound = true;
            }

            if (isFound)
                result = new ResponsePackage("bool", JsonConvert.SerializeObject(true));
            else
            {
                result = new ResponsePackage("error", JsonConvert.SerializeObject(new Error("User already disconected")));
            }

            ServerTCP.SendResponse(ServerData.ConnectedClients[currentUser], JsonConvert.SerializeObject(result));
            ServerData.ConnectedClients[currentUser].ClientTcp.Close();
            ServerData.ConnectedClients.RemoveAt(currentUser);
        }

        public static User FindUser(String login)
        {
            User resultUser = null;

            for (int i = 0; i < MockDB.Users.Count; i++)
            {
                if (login == MockDB.Users[i].Login)
                {
                    resultUser = MockDB.Users[i];
                    break;
                }
            }
                
            return resultUser;
        }
    }
}
