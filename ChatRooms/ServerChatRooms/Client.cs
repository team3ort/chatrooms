﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ChatModels;
using System.IO;
using System.Net.Sockets;

namespace ServerChatRooms
{
    public class Client
    {
        public Client(TcpClient client)
        {
            ClientTcp = client;
        }

        public TcpClient ClientTcp { get; set; }
        public User User { get; set; }

    }
}
