﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatModels;

namespace ServerChatRooms
{
    public static class MockDB
    {
        //public MockDB()
        //{
        //    Users.Add(new User("vasya", "12345"));
        //    ////
        //    Rooms.Add(new Room("Massons"));
        //    ///
        //    Messages.Add(new Message(Users[0], Rooms[0], "Hello"));
        //    //// Остальное в том же духе, пару комнат с юзерами и сообщениями.
        //}
        public static List<User> Users = new List<User>();
        public static List<Room> Rooms = new List<Room>();
        public static List<Message> Messages = new List<Message>();

        public static void Init()
        {
            User user0 = new User("user0", "user0", "FName0", "LName0", "mail0@gmail.com", "0000000");
            user0.Id = 0;
            Users.Add(user0);
            User user1 = new User("user1", "user1", "FName1", "LName1", "mail1@gmail.com", "1111111");
            user1.Id = 1;
            Users.Add(user1);
            User user2 = new User("user2", "user2", "FName2", "LName2", "mail2@gmail.com", "2222222");
            user2.Id = 2;
            Users.Add(user2);
            User user3 = new User("user3", "user3", "FName3", "LName3", "mail3@gmail.com", "3333333");
            user3.Id = 3;
            Users.Add(user3);
            User user4 = new User("user4", "user4", "FName4", "LName4", "mail4@gmail.com", "4444444");
            user4.Id = 4;
            Users.Add(user4);
            User user5 = new User("user5", "user5", "FName5", "LName5", "mail5@gmail.com", "5555555");
            user5.Id = 5;
            Users.Add(user5);
            User user6 = new User("user6", "user6", "FName6", "LName6", "mail6@gmail.com", "6666666");
            user6.Id = 6;
            Users.Add(user6);
            User user7 = new User("user7", "user7", "FName7", "LName7", "mail7@gmail.com", "7777777");
            user7.Id = 7;
            Users.Add(user7);
            User user8 = new User("user8", "user8", "FName8", "LName8", "mail8@gmail.com", "8888888");
            user8.Id = 8;
            Users.Add(user8);
            User user9 = new User("user9", "user9", "FName9", "LName9", "mail9@gmail.com", "9999999");
            user9.Id = 9;
            Users.Add(user9);
            User userTest0 = new User("usertest0", "usertest0", "FNusertest0", "LNusertest0", "mailusertest0@gmail.com", "usertest000000");
            userTest0.Id = 10;
            Users.Add(userTest0);
            User userTest1 = new User("usertest1", "usertest1", "FNusertest1", "LNusertest1", "mailusertest1@gmail.com", "usertest111111");
            userTest1.Id = 11;
            Users.Add(userTest1);
            User userTest2 = new User("usertest2", "usertest2", "FNusertest2", "LNusertest2", "mailusertest2@gmail.com", "usertest222222");
            userTest2.Id = 12;
            Users.Add(userTest2);

            Room room0 = new Room("room0");
            room0.Id = 0;
            room0.Creator = user0.getName();
            room0.UsersInRoom = new List<User>();
            room0.UsersInRoom.Add(user0.getName());
            room0.UsersInRoom.Add(user1.getName());
            room0.UsersInRoom.Add(user2.getName());
            room0.UsersInRoom.Add(user3.getName());
            room0.UsersInRoom.Add(user4.getName());
            room0.UsersInRoom.Add(user5.getName());
            room0.UsersInRoom.Add(user6.getName());
            room0.UsersInRoom.Add(user7.getName());

            Rooms.Add(room0);

            Room room1 = new Room("aroom1");
            room1.Id = 1;
            room1.Creator = user1.getName();
            room1.UsersInRoom = new List<User>();
            room1.UsersInRoom.Add(user2.getName());
            room1.UsersInRoom.Add(user3.getName());
            room1.UsersInRoom.Add(user4.getName());
            room1.UsersInRoom.Add(user5.getName());

            Rooms.Add(room1);

            Room room2 = new Room("abroom2");
            room2.Id = 2;
            room2.Creator = user2.getName();
            room2.UsersInRoom = new List<User>();
            room2.UsersInRoom.Add(user6.getName());
            room2.UsersInRoom.Add(user7.getName());
            room2.UsersInRoom.Add(user8.getName());
            room2.UsersInRoom.Add(user9.getName());

            Rooms.Add(room2);
            
            Messages.Add(new Message(user0, room0, "message000"));
            Messages.Add(new Message(user0, room0, "message001"));
            Messages.Add(new Message(user0, room0, "message002"));
            Messages.Add(new Message(user1, room0, "message100"));
            Messages.Add(new Message(user2, room0, "message200"));
            Messages.Add(new Message(user3, room0, "message300"));
            Messages.Add(new Message(user3, room0, "message301"));
            Messages.Add(new Message(user2, room1, "message210"));
            Messages.Add(new Message(user2, room1, "message211"));
            Messages.Add(new Message(user6, room2, "message620"));
        }
    }
}
