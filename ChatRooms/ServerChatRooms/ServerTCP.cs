﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace ServerChatRooms
{
    class ServerTCP
    {
        TcpListener listener;
        CommandParser cmdParser = new CommandParser();

        //http://sealaunch.ddns.net:8080/

        public ServerTCP(string host = "http://127.0.0.1:8080/")
        {
            MockDB.Init();
            ServerData.ConnectedClients = new List<Client>();
            //IPAddress ip = new IPAddress(0x00000000);
            listener = new TcpListener(IPAddress.Any, 11155);
            Thread listenConnectedClients = new Thread(ListenConnectedClients);
            listenConnectedClients.IsBackground = true;
            listenConnectedClients.Start();
        }

        public void StartServer()
        {
            listener.Start();
            Console.WriteLine("Server start");
            while (true)
            {
                var client = listener.AcceptTcpClient();

                Console.WriteLine("Client connect ID#{0}\n", ServerData.ConnectedClients.Count);
                ServerData.ConnectedClients.Add(new Client(client));
            }
        }

        private void ListenConnectedClients()
        {
            while (true)
            {
                for (int i = 0; i < ServerData.ConnectedClients.Count; i++)
                {
                    TcpClient client = ServerData.ConnectedClients[i].ClientTcp;
                    if (client.GetStream() != null)
                    {
                        if (client.Client.Poll(0, SelectMode.SelectRead) && client.Client.Available == 0)
                        {
                            client.Close();
                            Console.WriteLine("Client disconnect ID#{0}\n", i);

                            Dictionary<string, string> request;
                            request = new Dictionary<string, string>();
                            request.Add("cmd", "userdisconnect");          
                            request.Add("user", ServerData.ConnectedClients[i].User.Login);
                            string reqString = JsonConvert.SerializeObject(request);
                            cmdParser.Response(reqString, i);

                            ServerData.ConnectedClients.RemoveAt(i);   ///
                        }
                        else if (client.GetStream().DataAvailable)
                        {
                            string inputMessage = new BinaryReader(client.GetStream()).ReadString();
                            Console.WriteLine("Request: {0}\n", inputMessage);
                            cmdParser.Response(inputMessage, i);
                        }
                    }
                }

                Thread.Sleep(1000);
            }
        }

        public static void SendResponse(Client client, String response)
        {
            Stream responseStraem;
                        
            responseStraem = client.ClientTcp.GetStream();
            BinaryWriter writer = new BinaryWriter(responseStraem);
            //writer.AutoFlush = true;
            writer.Write(response);            
            writer.Flush();
            //writer.Dispose();
            Console.WriteLine("Response: {0}\n", response);
            // Проверить            
        }
    }
}