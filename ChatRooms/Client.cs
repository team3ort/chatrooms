﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ChatRooms
{
    class Client
    {
        string response;
        Dictionary<string, string> request;

        public Client()
        {

        }

        public Client(Dictionary<string, string> request)
        {
            this.request = request;
        }

        private void SendRequest()
        {
            string url = "http://sealaunch.ddns.net/";
            //string url = "http://127.0.0.1";
            var content = new FormUrlEncodedContent(request);

            using (var client = new HttpClient())
            {
                try
                {
                    var httpResponseMessage = client.PostAsync(url, content).Result;
                    
                    if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                    {
                        Response = httpResponseMessage.Content.ReadAsStringAsync().Result;
                    }
                }
                catch (Exception e) { }
            }
        }

        public string GetResponse()
        {
            SendRequest();
            return Response;
        }

        string Response
        {
            get
            {
                return response;
            }

            set
            {
                response = value;
            }
        }
    }
}
